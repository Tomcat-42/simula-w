var game = new Phaser.Game(900, 695, Phaser.CANVAS, 'phaser-example', { preload: preload, create: create, update: update, render: render });

function preload() {

    game.load.image('grid', 'imagens/grid.png');
    game.load.image('fundo', 'imagens/fundo.png');
    game.load.image('pizza', 'imagens/pizza-quarto.png');
    //formas
    game.load.image('umSexto1', 'imagens/formas/1_6_1.png');
    game.load.image('umSexto2', 'imagens/formas/1_6_2.png');
    game.load.image('umQuarto1', 'imagens/formas/1_4_1.png');
    game.load.image('umQuarto2', 'imagens/formas/1_4_2.png');
    game.load.image('umMeio', 'imagens/formas/1_2.png');
    game.load.image('umTerco', 'imagens/formas/1_3.png');
}

var group1;

function create() {

    var fundo = game.add.sprite(0, 0, 'fundo');
    var grid = game.add.sprite(300, 140, 'grid');
    //fundo.width = 800;

    group1 = game.add.group();
    
    var umSexto1 = game.add.sprite(304, 558, 'umSexto1');
    umSexto1.name = 'umSexto1';
    umSexto1.inputEnabled = true;
    umSexto1.input.enableDrag(false, true);
    group1.add(umSexto1);
    
    var umSexto2 = game.add.sprite(443, 559, 'umSexto2');
    umSexto2.name = 'umSexto2';
    umSexto2.inputEnabled = true;
    umSexto2.input.enableDrag(false, true);
    group1.add(umSexto2);
    
    var umQuarto1 = game.add.sprite(585, 350, 'umQuarto1');
    umQuarto1.name = 'umQuarto1';
    umQuarto1.inputEnabled = true;
    umQuarto1.input.enableDrag(false, true);
    group1.add(umQuarto1);
    
    var umQuarto2 = game.add.sprite(585, 145, 'umQuarto2');
    umQuarto2.name = 'umQuarto2';
    umQuarto2.inputEnabled = true;
    umQuarto2.input.enableDrag(false, true);
    group1.add(umQuarto2);
    
    var umMeio = game.add.sprite(160, 146, 'umMeio');
    umMeio.name = 'umMeio';
    umMeio.inputEnabled = true;
    umMeio.input.enableDrag(false, true);
    group1.add(umMeio);
    
    var umTerco = game.add.sprite(304, 3, 'umTerco');
    umTerco.name = 'umTerco';
    umTerco.inputEnabled = true;
    umTerco.input.enableDrag(false, true);
    group1.add(umTerco); 
}

function update() {

}

function render() {
    
}