<?php
include_once('../../utils/funcoes.php');

class GraficosQuantidadesMosquitosWolbachia{
    var $arquivo;
    var $nome_arquivo;
    private static $dados = array();

    public function __construct($nmarquivo){

        self::$dados = $nmarquivo;

    }

    function criar_grafico_sexo(){
        $ret;

        $ret[0]["title"]    = "Sexos Mosquitos Visao Wolbachia";
        $ret[0]["legendas"] = ["Machos","Femeas"];
        $ret[0]["cores"]    = ["black","black"];

        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 1  , 11 , 1);
        $ret[0]["valores"][1] = copia_m_v_sum(self::$dados, 11 , 21 , 1);

        return $ret;
    }

    function criar_grafico_fase(){
        $ret;

        $ret[0]["title"]    = "Fases Mosquitos Visao Wolbachia";
        $ret[0]["legendas"] = ["Ovos","Larvas","Pupas","Ativos","Decadentes"];
        $ret[0]["cores"]    = ["blue","blue","blue","red","red"];

        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 1  , 21 , 10);
        $ret[0]["valores"][1] = copia_m_v_sum(self::$dados, 3  , 21 , 10);
        $ret[0]["valores"][2] = copia_m_v_sum(self::$dados, 5  , 21 , 10);
        $ret[0]["valores"][3] = copia_m_v_sum(self::$dados, 7  , 21 , 10);
        $ret[0]["valores"][4] = copia_m_v_sum(self::$dados, 9  , 21 , 10);

        return $ret;
    }

    function criar_grafico_saude(){
        $ret;

        $ret[0]["title"]    = "Saude Mosquitos Visao Wolbachia";
        $ret[0]["legendas"] = ["Saudaveis","Infectados"];
        $ret[0]["cores"]    = ["green","red"];

        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 1  , 21 , 2);
        $ret[0]["valores"][1] = copia_m_v_sum(self::$dados, 2  , 21 , 2);

        return $ret;
    }

    function criar_grafico_sexo_saude(){
        $ret;

        $ret[0]["title"]    = "Sexos e Saude Mosquitos Visao Wolbachia";
        $ret[0]["legendas"] = ["Machos Saudaveis","Machos Infectados","Femeas Saudaveis","Femeas Infectadas"];
        $ret[0]["cores"]    = ["green","red","green","red"];

        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 1   , 11 , 2);
        $ret[0]["valores"][1] = copia_m_v_sum(self::$dados, 2   , 11 , 2);
        $ret[0]["valores"][2] = copia_m_v_sum(self::$dados, 11  , 11 , 2);
        $ret[0]["valores"][3] = copia_m_v_sum(self::$dados, 12  , 11 , 2);

        return $ret;
    }

    function criar_grafico_sexo_fase(){
        $ret;

        $ret[0]["title"]    = "Sexos e Fases Mosquitos Visao Wolbachia";
        $ret[0]["legendas"] = ["Machos Ovos","Femeas Ovos","Machos Larvas","Femeas Larvas","Machos Pupas","Femeas Pupas","Machos Ativos","Femeas Ativas","Machos Decadentes","Femeas Decadentes"];
        $ret[0]["cores"]    = ["ble","magenta","ble","magenta","ble","magenta","ble","magenta","ble","magenta"];

        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 1   , 3  , 1);
        $ret[0]["valores"][2] = copia_m_v_sum(self::$dados, 3   , 5  , 1);
        $ret[0]["valores"][4] = copia_m_v_sum(self::$dados, 5   , 7  , 1);
        $ret[0]["valores"][6] = copia_m_v_sum(self::$dados, 7   , 9  , 1);
        $ret[0]["valores"][8] = copia_m_v_sum(self::$dados, 9   , 11 , 1);

        $ret[0]["valores"][1] = copia_m_v_sum(self::$dados, 11   , 13 , 1);
        $ret[0]["valores"][3] = copia_m_v_sum(self::$dados, 13   , 15 , 1);
        $ret[0]["valores"][5] = copia_m_v_sum(self::$dados, 15   , 17 , 1);
        $ret[0]["valores"][7] = copia_m_v_sum(self::$dados, 17   , 19 , 1);
        $ret[0]["valores"][9] = copia_m_v_sum(self::$dados, 19   , 21 , 1);

        return $ret;
    }

    function criar_grafico_saude_fase(){
        $ret;

        $ret[0]["title"]    = "Saude Mosquitos Nao Alados Visao Wolbachia";
        $ret[0]["legendas"] = ["Ovos Saudaveis","Ovos Infectados","Larvas Saudaveis","Larvas Infectados","Pupas Saudaveis","Pupas Infectados"];
        $ret[0]["cores"]    = ["green","red","green","red","green","red"];

        $ret[1]["title"]    = "Saude Mosquitos Alados Visao Wolbachia";
        $ret[1]["legendas"] = ["Ativos Saudaveis","Ativos Infectados","Decadentes Saudaveis","Decadentes Infectados"];
        $ret[1]["cores"]    = ["green","red","green","red"];

        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 1   , 21 , 10);
        $ret[0]["valores"][2] = copia_m_v_sum(self::$dados, 3   , 21 , 10);
        $ret[0]["valores"][4] = copia_m_v_sum(self::$dados, 5   , 21 , 10);
        $ret[1]["valores"][0] = copia_m_v_sum(self::$dados, 7   , 21 , 10);
        $ret[1]["valores"][2] = copia_m_v_sum(self::$dados, 9   , 21 , 10);

        $ret[0]["valores"][1] = copia_m_v_sum(self::$dados, 2   , 21 , 10);
        $ret[0]["valores"][3] = copia_m_v_sum(self::$dados, 4   , 21 , 10);
        $ret[0]["valores"][5] = copia_m_v_sum(self::$dados, 6   , 21 , 10);
        $ret[1]["valores"][1] = copia_m_v_sum(self::$dados, 8   , 21 , 10);
        $ret[1]["valores"][3] = copia_m_v_sum(self::$dados, 10  , 21 , 10);

        return $ret;
    }
}

?>
