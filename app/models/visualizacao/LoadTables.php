<?php

    //carrega todos os mapas
    include_once("../conecta.php");

    header('Content-type:'."text/plain");

    // query que pega as tabelas e o numero de resultados
        $sql         = "SELECT TABLE_NAME AS TABLENAME
                        FROM
                            INFORMATION_SCHEMA.COLUMNS   
                        WHERE
                            COLUMN_NAME LIKE 'geom%'
                        OR
                            COLUMN_NAME ='x'   
                        ORDER BY
                            TABLENAME ASC";
    $QueryTables = pg_query($conn,$sql);
    $Ntabelas    = pg_numrows($QueryTables);

    //gera o json
    for( $i = 0 ; $i < $Ntabelas ; $i++){
        $tabelas[] = pg_fetch_assoc($QueryTables, $i);
    }
    $jsonTabelas = json_encode($tabelas, JSON_PRETTY_PRINT);

    print $jsonTabelas;
?>
