<?php
    include_once("../../conecta.php");

    $layer = $_POST['layer'];

    //verifica se a tabela é um mapa ou um conjunto de pontos
    $queryGeom = "SELECT column_name FROM(SELECT column_name FROM information_schema.columns WHERE table_name ='$layer') AS t WHERE t.column_name = 'geom'";
    $vertipo = pg_query($conn,$queryGeom);
    $nrow = pg_numrows($vertipo);
    if($nrow > 0)
    {
        $sqlTipoGeom = "select ST_AsText(geom) from $layer";
        $queryTipoGeom = pg_query($conn,$sqlTipoGeom);
        $first = pg_fetch_array($queryTipoGeom);
        $first = substr($first[0],0,12);
        if($first == "MULTIPOLYGON"){
            echo json_encode("POLIGONO",JSON_PRETTY_PRINT);
        }
        else
            echo json_encode("PONTO",JSON_PRETTY_PRINT);
    }
    else
       echo json_encode("PONTO",JSON_PRETTY_PRINT);
?>