<?php
    include_once("../../../conecta.php");

    $sql = "SELECT TABLE_NAME AS TABLENAME
            FROM
                INFORMATION_SCHEMA.COLUMNS   
            WHERE
                COLUMN_NAME = 'quadra'
            OR
                COLUMN_NAME = 'loc'   
            ORDER BY
                TABLENAME ASC;";
    $query = pg_query($conn, $sql);

    $ntables = pg_numrows($query);
    $tabelas = NULL;

    for( $i = 0 ; $i < $ntables ; $i++){
        $tabelas[] = pg_fetch_assoc($query, $i);
    }
    echo json_encode($tabelas, JSON_PRETTY_PRINT);
?>