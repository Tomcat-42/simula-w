<?php

function getColunasTabela($conn, $tabela){
    $sql = "SELECT 
                column_name 
            FROM 
                information_schema.columns 
            WHERE 
                table_name = '$tabela'
            AND
                column_name != 'quadra'
            AND
                column_name != 'loc';";

    $query = pg_query($conn, $sql);
    $ntcol = pg_numrows($query);
    $colunas = NULL;
    $resposta = [];

    for($i = 0 ; $i < $ntcol ; $i++)
        $colunas[] = pg_fetch_assoc($query, $i);

    for($i = 0 ; $i < sizeof($colunas) ; $i++){
        $resposta[$i] = $colunas[$i]['column_name'];
    }
    return $resposta;
}

function getQuadrasTabela($conn, $tabela){
    $sql = "SELECT 
                quadra 
            FROM 
                $tabela";
    $query = pg_query($conn, $sql);
    $nquadras = pg_numrows($query);
    $quadras = NULL;
    $resposta = [];

    for($i = 0 ; $i < $nquadras ; $i++)
        $quadras[] = pg_fetch_assoc($query, $i);
    for($i = 0 ; $i < sizeof($quadras) ; $i++){
        $resposta[$i] = $quadras[$i]['quadra'];
    }
    return $resposta;
}

function getDadosColunaTabela($conn, $tabela, $coluna){
    $sql = "SELECT 
                $coluna 
            FROM 
                $tabela";
    $query = pg_query($conn, $sql);
    $nquadras = pg_numrows($query);
    $quadras = NULL;
    $resposta = [];

    for($i = 0 ; $i < $nquadras ; $i++)
        $quadras[] = pg_fetch_assoc($query, $i);
    for($i = 0 ; $i < sizeof($quadras) ; $i++){
        $resposta[$i] = $quadras[$i][''.$coluna.''];
    }
    return $resposta;
}





?>