<?php
    include_once("../../../conecta.php");
    include_once("funcoes.php");
    
    $tabela = $_POST['tabela'];

    $resposta = [];

    $colunas = getColunasTabela($conn, $tabela);
    $quadras = getQuadrasTabela($conn, $tabela);
   
    $resposta['colunas'] = $colunas;

    $resposta['quadras'] = $quadras;

    echo json_encode($resposta, JSON_PRETTY_PRINT);
?>