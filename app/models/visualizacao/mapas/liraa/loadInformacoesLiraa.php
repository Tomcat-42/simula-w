<?php
    include_once("../../../conecta.php");
    include_once("funcoes.php");
    
    $tabela = $_POST['tabela'];
    $coluna = $_POST['coluna'];

    $resposta = getDadosColunaTabela($conn, $tabela, $coluna);

    echo json_encode($resposta, JSON_PRETTY_PRINT);
?>