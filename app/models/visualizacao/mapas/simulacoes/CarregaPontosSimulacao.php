<?php
    //retorna pontos da simulação.
    include_once("../../../conecta-simula.php");

    $arquivo = $_POST['arquivo'];
    $ciclo = $_POST['ciclo'];

    //caso não sejam requisitados individuos especificos retorna todos os individuos do ciclo;
    if(!isset($_POST['individuos']))
        $sql = "SELECT regexp_replace(ST_Astext(ST_Transform( ST_SetSRID(st_makepoint(x,y),31982) , 3857)), '[A-Z()]', '', 'g') as coord,c$ciclo as icone FROM $arquivo WHERE c$ciclo != 0";
    else
    {
        $individuos = $_POST['individuos'];
        $sql = "SELECT regexp_replace(ST_Astext(ST_Transform( ST_SetSRID(st_makepoint(x,y),31982) , 3857)), '[A-Z()]', '', 'g') as coord,c$ciclo as icone FROM $arquivo WHERE ";
        for($i = 0 ; $i < sizeof($individuos) ; $i++)
        {
            $sql.="c$ciclo = '".$individuos[$i]['cod']."'";
            if($i < sizeof($individuos)-1)
                $sql.=" or ";
        }
    }
    //echo $sql;
    $pontos = pg_query($conn_simula,$sql);

    $nlinhas = pg_numrows($pontos);

    $dados = [];
    for($i = 0 ; $i < $nlinhas ; $i++)
        $dados [] = pg_fetch_assoc($pontos, $i);
    echo json_encode($dados, JSON_PRETTY_PRINT);
?>
