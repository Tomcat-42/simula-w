<?php
    include_once("../../../conecta-simula.php");

    $sql = "SELECT * from graficos_simulacao";
    $tabelas;

    $query = pg_query($conn_simula,$sql);
    $ntables = pg_numrows($query);
    $tabelas = NULL;

    for( $i = 0 ; $i < $ntables ; $i++){
        $tabelas[] = pg_fetch_assoc($query, $i);
    }
    echo json_encode($tabelas, JSON_PRETTY_PRINT);
?>
