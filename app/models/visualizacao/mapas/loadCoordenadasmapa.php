<?php
/*
*   Retorna as geos dos poligonos do mapa, o mslinks e o centro do mapa 
*
*   18/03/2018
*/
    function GeraCoordenadaMapa($layer,$conn)
    {
        header("Content-Type: text/plain");
        $sql = "select 
                gid, quadra, regexp_replace(ST_Astext(ST_Transform( ST_SetSRID(geom,31982) , 3857)), '[A-Z()]', '', 'g') ,
                st_x(st_centroid(ST_Transform( ST_SetSRID(geom,31982) , 3857))),
                st_y(st_centroid(ST_Transform( ST_SetSRID(geom,31982) , 3857))) from $layer ";
        $qry = pg_query($conn,$sql);
        $nlinhas = pg_numrows($qry);
        if($nlinhas > 0){
            $dados = [];
            for($i = 0 ; $i < $nlinhas ; $i++){
                $dados [] = pg_fetch_assoc($qry, $i);
            }
            
            echo json_encode($dados, JSON_PRETTY_PRINT);
        }
        else{
            echo "Erro na consulta";
        }
    }
?>