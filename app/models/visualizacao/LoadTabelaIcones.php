<?php

    // retorna relação entre os codigos dos icones dos agentes e seus nomes;
    include_once("../conecta-simula.php");

    header('Content-type:'."text/plain");

    $sql         = "select * from icones_agentes";
    $QueryIcones = pg_query($conn_simula,$sql);
    $NIcones    = pg_numrows($QueryIcones);

    //gera o json
    for( $i = 0 ; $i < $NIcones ; $i++){
        $Icones[] = pg_fetch_assoc($QueryIcones, $i);
    }
    $jsonIcones = json_encode($Icones/*, JSON_PRETTY_PRINT*/);

    print $jsonIcones;
?>
