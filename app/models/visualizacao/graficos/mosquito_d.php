<?php
include_once('../../utils/funcoes.php');

class GraficosQuantidadesMosquitosDengue{
    var $arquivo;
    var $nome_arquivo;
    private static $dados = array();

    public function __construct($nmarquivo){

        self::$dados = $nmarquivo;

    }

    function criar_grafico_fase(){
        $ret;

        $ret[0]["title"]    = "Fases Mosquitos Visao Dengue";
        $ret[0]["legendas"] = ["Ovos","Larvas","Pupas","Ativos","Decadentes"];
        $ret[0]["cores"]    = ["blue","blue","blue","blue","red","red"];

        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 1  , 151 , 75);
        $ret[0]["valores"][1] = copia_m_v_sum(self::$dados, 16 , 151 , 75);
        $ret[0]["valores"][2] = copia_m_v_sum(self::$dados, 31 , 151 , 75);
        $ret[0]["valores"][3] = copia_m_v_sum(self::$dados, 46 , 151 , 75);
        $ret[0]["valores"][4] = copia_m_v_sum(self::$dados, 61 , 151 , 75);

        return $ret;
    }

    function criar_grafico_sexo(){
        $ret;

        $ret[0]["title"]    = "Sexos Mosquitos Visao Dengue";
        $ret[0]["legendas"] = ["Machos","Femeas"];
        $ret[0]["cores"]    = ["black","black"];

        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 1   , 76  , 1);
        $ret[0]["valores"][1] = copia_m_v_sum(self::$dados, 76  , 151 , 1);

        return $ret;
    }

    function criar_grafico_saude(){
        $ret;

        $ret[0]["title"]    = "Saude Mosquitos Visao Dengue";
        $ret[0]["legendas"] = ["Saudaveis","Expostos","Infectados"];
        $ret[0]["cores"]    = ["green","brown","red"];

        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 1  , 151 , 15);
        $ret[0]["valores"][1] = copia_m_v_sum(self::$dados, 6  , 151 , 15);
        $ret[0]["valores"][2] = copia_m_v_sum(self::$dados, 11 , 151 , 15);

        return $ret;
    }

    function criar_grafico_sorotipo(){
        $ret;

        $ret[0]["title"]    = "Sorotipos Mosquitos Visao Dengue";
        $ret[0]["legendas"] = ["Sucetiveis","Infectados Sorotipo 1","Infectados Sorotipo 2","Infectados Sorotipo 3","Infectados Sorotipo 4"];
        $ret[0]["cores"]    = ["grren","red","LightCoral","magenta","brown"];

        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 1  , 151 , 5);
        $ret[0]["valores"][1] = copia_m_v_sum(self::$dados, 2  , 151 , 5);
        $ret[0]["valores"][2] = copia_m_v_sum(self::$dados, 3  , 151 , 5);
        $ret[0]["valores"][3] = copia_m_v_sum(self::$dados, 4  , 151 , 5);
        $ret[0]["valores"][4] = copia_m_v_sum(self::$dados, 5  , 151 , 5);

        return $ret;
    }

    function criar_grafico_saude_sorotipo(){
        $ret;

        $ret[0]["title"]    = "Saude Mosquitos Sorotipo 1 Visao Dengue";
        $ret[0]["legendas"] = ["Sucetiveis Sorotipo 1","Expostos Sorotipo 1","Infectados Sorotipo 1"];
        $ret[0]["cores"]    = ["grren","red","LightCoral","magenta","brown"];

        $ret[1]["title"]    = "Saude Mosquitos Sorotipo 2 Visao Dengue";
        $ret[1]["legendas"] = ["Sucetiveis Sorotipo 1","Expostos Sorotipo 1","Infectados Sorotipo 1"];
        $ret[1]["cores"]    = ["grren","red","LightCoral","magenta","brown"];

        $ret[2]["title"]    = "Saude Mosquitos Sorotipo 3 Visao Dengue";
        $ret[2]["legendas"] = ["Sucetiveis Sorotipo 1","Expostos Sorotipo 1","Infectados Sorotipo 1"];
        $ret[2]["cores"]    = ["grren","red","LightCoral","magenta","brown"];

        $ret[3]["title"]    = "Saude Mosquitos Sorotipo 4 Visao Dengue";
        $ret[3]["legendas"] = ["Sucetiveis Sorotipo 1","Expostos Sorotipo 1","Infectados Sorotipo 1"];
        $ret[3]["cores"]    = ["grren","red","LightCoral","magenta","brown"];

        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 2  , 151 , 15);
        $ret[1]["valores"][0] = copia_m_v_sum(self::$dados, 3  , 151 , 15);
        $ret[2]["valores"][0] = copia_m_v_sum(self::$dados, 4  , 151 , 15);
        $ret[3]["valores"][0] = copia_m_v_sum(self::$dados, 5  , 151 , 15);

        $ret[0]["valores"][1] = copia_m_v_sum(self::$dados, 7   , 151 , 15);
        $ret[1]["valores"][1] = copia_m_v_sum(self::$dados, 8   , 151 , 15);
        $ret[2]["valores"][1] = copia_m_v_sum(self::$dados, 9   , 151 , 15);
        $ret[3]["valores"][1] = copia_m_v_sum(self::$dados, 10  , 151 , 15);

        $ret[0]["valores"][2] = copia_m_v_sum(self::$dados, 12  , 151 , 15);
        $ret[1]["valores"][2] = copia_m_v_sum(self::$dados, 13  , 151 , 15);
        $ret[2]["valores"][2] = copia_m_v_sum(self::$dados, 14  , 151 , 15);
        $ret[3]["valores"][2] = copia_m_v_sum(self::$dados, 15  , 151 , 15);

        return $ret;
    }

    function criar_grafico_saude_fase(){
        $ret;

        $ret[0]["title"]    = "Fases e Saude Mosquitos Visao Dengue";
        $ret[0]["legendas"] = ["Ovos Sucetiveis","Ovos Expostos","Ovos Infectados","Larvas Sucetiveis","Larvas Expostos","Larvas Infectados","Pupas Sucetiveis","Pupas Expostos","Pupas Infectados","Ativos Sucetiveis","Ativos Expostos","Ativos Infectados","Decadentes Sucetiveis","Decadentes Expostos","Decadentes Infectados"];
        $ret[0]["cores"]    = ["grren","brown","red","grren","brown","red","grren","brown","red","grren","brown","red","grren","brown","red"];

        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 1   , 151 , 75);
        $ret[0]["valores"][1] = copia_m_v_sum(self::$dados, 6   , 151 , 75);
        $ret[0]["valores"][2] = copia_m_v_sum(self::$dados, 11  , 151 , 75);

        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 15  , 151 , 75);
        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 15  , 151 , 75);
        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 15  , 151 , 75);

        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 15  , 151 , 75);
        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 15  , 151 , 75);
        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 15  , 151 , 75);

        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 15  , 151 , 75);
        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 15  , 151 , 75);
        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 15  , 151 , 75);

        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 15  , 151 , 75);
        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 15  , 151 , 75);
        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 15  , 151 , 75);

        return $ret;
    }

    function criar_grafico_sexo_fase(){
        $ret;

        $ret[0]["title"]    = "Sexos e Fases Mosquitos Visao Dengue";
        $ret[0]["legendas"] = ["Machos Ovos","Femeas Ovos","Machos Larvas","Femeas Larvas","Machos Pupas","Femeas Pupas","Machos Ativos","Femeas Ativos","Machos Decadentes","Femeas Decadentes"];
        $ret[0]["cores"]    = ["blue","blue","blue","blue","blue","blue","red","red","red","red"];

        $ret[0]["valores"][1] = copia_m_v_sum(self::$dados, 1  , 16 , 1);
        $ret[0]["valores"][3] = copia_m_v_sum(self::$dados, 16  , 16 , 1);
        $ret[0]["valores"][5] = copia_m_v_sum(self::$dados, 31  , 16 , 1);
        $ret[0]["valores"][7] = copia_m_v_sum(self::$dados, 46  , 16 , 1);
        $ret[0]["valores"][9] = copia_m_v_sum(self::$dados, 61  , 16 , 1);

        $ret[0]["valores"][2] = copia_m_v_sum(self::$dados,  76  , 16 , 1);
        $ret[0]["valores"][4] = copia_m_v_sum(self::$dados,  91  , 16 , 1);
        $ret[0]["valores"][6] = copia_m_v_sum(self::$dados,  106 , 16 , 1);
        $ret[0]["valores"][8] = copia_m_v_sum(self::$dados,  121 , 16 , 1);
        $ret[0]["valores"][10] = copia_m_v_sum(self::$dados, 136 , 16 , 1);

        return $ret;
    }
}

?>
