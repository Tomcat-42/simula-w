<?php
include_once('../../utils/funcoes.php');

class GraficosQuantidadesHumanos{
    var $arquivo;
    var $nome_arquivo;
    private static $dados = array();

    public function __construct($nmarquivo){

        self::$dados = $nmarquivo;
    }

    function criar_grafico_saude(){ 

        $ret;

        $ret[0]["title"] = "Saude Humanos";
        $ret[0]["legendas"] = ["Sucetiveis","Expostos","Infectados","Hemorragicos","Imunizados","Recuperados"];
        $ret[0]["cores"] =["green","brown","red","LightCoral","magenta","blue"];

        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 1 , 361 , 30);
        $ret[0]["valores"][1] = copia_m_v_sum(self::$dados, 6 , 361 , 30);
        $ret[0]["valores"][2] = copia_m_v_sum(self::$dados, 11 , 361 , 30);
        $ret[0]["valores"][3] = copia_m_v_sum(self::$dados, 16 , 361 , 30);
        $ret[0]["valores"][4] = copia_m_v_sum(self::$dados, 21 , 361 , 30);
        $ret[0]["valores"][5] = copia_m_v_sum(self::$dados, 26 , 361 , 30);

        return $ret;
    }

    function criar_grafico_faixa_etaria(){

        $ret;

        $ret[0]["title"]    = "Faixas Etarias Humanos";
        $ret[0]["legendas"] = ["Bebes","Criancas","Adolescentes","Jovens","Adultos","Idosos"];
        $ret[0]["cores"]    = ["blue","green","LightCoral","Brown","red","magenta"];

        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 1 , 361 , 180);
        $ret[0]["valores"][1] = copia_m_v_sum(self::$dados, 31 , 361 , 180);
        $ret[0]["valores"][2] = copia_m_v_sum(self::$dados, 61 , 361 , 180);
        $ret[0]["valores"][3] = copia_m_v_sum(self::$dados, 91 , 361 , 180);
        $ret[0]["valores"][4] = copia_m_v_sum(self::$dados, 121 , 361 , 180);
        $ret[0]["valores"][5] = copia_m_v_sum(self::$dados, 151 , 361 , 180);

        return $ret;

    }

    function criar_grafico_sexo(){

        $ret;

        $ret[0]["title"]    = "Sexos Humanos";
        $ret[0]["legendas"] = array("Masculinos","Femininos");
        $ret[0]["cores"]    = array("black","black");

        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 1   , 181 , 1);
        $ret[0]["valores"][1] = copia_m_v_sum(self::$dados, 181 , 361 , 1);

        return $ret;
    }

    function criar_grafico_sorotipo(){

        $ret;

        $ret[0]["title"]    = "Sorotipos Humanos";
        $ret[0]["legendas"] = array("Suscetiveis","Infectados Sorotipo 1","Infectados Sorotipo 2","Infectados Sorotipo 3","Infectados Sorotipo 4");
        $ret[0]["cores"]    = array("green","red","LightCoral","magenta","brown");

        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 1 , 361 , 5);
        $ret[0]["valores"][1] = copia_m_v_sum(self::$dados, 2 , 361 , 5);
        $ret[0]["valores"][2] = copia_m_v_sum(self::$dados, 3 , 361 , 5);
        $ret[0]["valores"][3] = copia_m_v_sum(self::$dados, 4 , 361 , 5);
        $ret[0]["valores"][4] = copia_m_v_sum(self::$dados, 5 , 361 , 5);

        return $ret;
    }

    function criar_grafico_saude_faixa_etaria(){

        $ret;

        $ret[0]["title"]    = "Saude Humanos Bebes";
        $ret[0]["legendas"] = array("Bebes Suscetiveis","Bebes Expostos","Bebes Infectados","Bebes Hemorragicos","Bebes Imunizados","Bebes Recuperados");
        $ret[0]["cores"]    = array("green","brown","red","LightCoral","magenta","blue");

        $ret[1]["title"]    = "Saude Humanos Criancas";
        $ret[1]["legendas"] = array("Criancas Suscetiveis","Criancas Expostos","Criancas Infectados","Criancas Hemorragicos","Criancas Imunizados","Criancas Recuperados");
        $ret[1]["cores"]    = array("green","brown","red","LightCoral","magenta","blue");

        $ret[2]["title"]    = "Saude Humanos Adolescentes";
        $ret[2]["legendas"] = array("Adolescentes Suscetiveis","Adolescentes Expostos","Adolescentes Infectados","Adolescentes Hemorragicos","Adolescentes Imunizados","Adolescentes Recuperados");
        $ret[2]["cores"]    = array("green","brown","red","LightCoral","magenta","blue");

        $ret[3]["title"]    = "Saude Humanos Jovens";
        $ret[3]["legendas"] = array("Jovens Suscetiveis","Jovens Expostos","Jovens Infectados","Jovens Hemorragicos","Jovens Imunizados","Jovens Recuperados");
        $ret[3]["cores"]    = array("green","brown","red","LightCoral","magenta","blue");

        $ret[4]["title"]    = "Saude Humanos Adultos";
        $ret[4]["legendas"] = array("Adultos Suscetiveis","Adultos Expostos","Adultos Infectados","Adultos Hemorragicos","Adultos Imunizados","Adultos Recuperados");
        $ret[4]["cores"]    = array("green","brown","red","LightCoral","magenta","blue");

        $ret[5]["title"]    = "Saude Humanos Idosos";
        $ret[5]["legendas"] = array("Idosos Suscetiveis","Idosos Expostos","Idosos Infectados","Idosos Hemorragicos","Idosos Imunizados","Idosos Recuperados");
        $ret[5]["cores"]    = array("green","brown","red","LightCoral","magenta","blue");


        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 1 , 361 , 180);
        $ret[0]["valores"][1] = copia_m_v_sum(self::$dados, 6 , 361 , 180);
        $ret[0]["valores"][2] = copia_m_v_sum(self::$dados, 11 , 361 , 180);
        $ret[0]["valores"][3] = copia_m_v_sum(self::$dados, 16 , 361 , 180);
        $ret[0]["valores"][4] = copia_m_v_sum(self::$dados, 21 , 361 , 180);
        $ret[0]["valores"][5] = copia_m_v_sum(self::$dados, 26 , 361 , 180);

        $ret[1]["valores"][0] = copia_m_v_sum(self::$dados, 31 , 361 , 180);
        $ret[1]["valores"][1] = copia_m_v_sum(self::$dados, 36 , 361 , 180);
        $ret[1]["valores"][2] = copia_m_v_sum(self::$dados, 41 , 361 , 180);
        $ret[1]["valores"][3] = copia_m_v_sum(self::$dados, 46 , 361 , 180);
        $ret[1]["valores"][4] = copia_m_v_sum(self::$dados, 51 , 361 , 180);
        $ret[1]["valores"][5] = copia_m_v_sum(self::$dados, 56 , 361 , 180);

        $ret[2]["valores"][0] = copia_m_v_sum(self::$dados, 61 , 361 , 180);
        $ret[2]["valores"][1] = copia_m_v_sum(self::$dados, 66 , 361 , 180);
        $ret[2]["valores"][2] = copia_m_v_sum(self::$dados, 71 , 361 , 180);
        $ret[2]["valores"][3] = copia_m_v_sum(self::$dados, 76 , 361 , 180);
        $ret[2]["valores"][4] = copia_m_v_sum(self::$dados, 81 , 361 , 180);
        $ret[2]["valores"][5] = copia_m_v_sum(self::$dados, 86 , 361 , 180);

        $ret[3]["valores"][0] = copia_m_v_sum(self::$dados, 91 , 361 , 180);
        $ret[3]["valores"][1] = copia_m_v_sum(self::$dados, 96 , 361 , 180);
        $ret[3]["valores"][2] = copia_m_v_sum(self::$dados, 101 , 361 , 180);
        $ret[3]["valores"][3] = copia_m_v_sum(self::$dados, 106 , 361 , 180);
        $ret[3]["valores"][4] = copia_m_v_sum(self::$dados, 111 , 361 , 180);
        $ret[3]["valores"][5] = copia_m_v_sum(self::$dados, 116 , 361 , 180);

        $ret[4]["valores"][0] = copia_m_v_sum(self::$dados, 121 , 361 , 180);
        $ret[4]["valores"][1] = copia_m_v_sum(self::$dados, 126 , 361 , 180);
        $ret[4]["valores"][2] = copia_m_v_sum(self::$dados, 131 , 361 , 180);
        $ret[4]["valores"][3] = copia_m_v_sum(self::$dados, 136 , 361 , 180);
        $ret[4]["valores"][4] = copia_m_v_sum(self::$dados, 141 , 361 , 180);
        $ret[4]["valores"][5] = copia_m_v_sum(self::$dados, 146 , 361 , 180);

        $ret[5]["valores"][0] = copia_m_v_sum(self::$dados, 151 , 361 , 180);
        $ret[5]["valores"][1] = copia_m_v_sum(self::$dados, 156 , 361 , 180);
        $ret[5]["valores"][2] = copia_m_v_sum(self::$dados, 161 , 361 , 180);
        $ret[5]["valores"][3] = copia_m_v_sum(self::$dados, 166 , 361 , 180);
        $ret[5]["valores"][4] = copia_m_v_sum(self::$dados, 171 , 361 , 180);
        $ret[5]["valores"][5] = copia_m_v_sum(self::$dados, 176 , 361 , 180);

        return $ret;
    }

    function criar_grafico_saude_sorotipo(){

        $ret;

        $ret[0]["title"]    = "Saude Humanos Sorotipo 1";
        $ret[0]["legendas"] = array("Suscetiveis Sorotipo 1","Expostos Sorotipo 1","Infectados Sorotipo 1"," Hemorragicos Sorotipo 1","Imunizados Sorotipo 1","Recuperados Sorotipo 1");
        $ret[0]["cores"]    = array("green","brown","red","LightCoral","magenta","blue");

        $ret[1]["title"]    = "Saude Humanos Sorotipo 2";
        $ret[1]["legendas"] = array("Suscetiveis Sorotipo 2","Expostos Sorotipo 2","Infectados Sorotipo 2"," Hemorragicos Sorotipo 2","Imunizados Sorotipo 2","Recuperados Sorotipo 2");
        $ret[1]["cores"]    = array("green","brown","red","LightCoral","magenta","blue");

        $ret[2]["title"]    = "Saude Humanos Sorotipo 3";
        $ret[2]["legendas"] = array("Suscetiveis Sorotipo 3","Expostos Sorotipo 3","Infectados Sorotipo 3"," Hemorragicos Sorotipo 3","Imunizados Sorotipo 3","Recuperados Sorotipo 3");
        $ret[2]["cores"]    = array("green","brown","red","LightCoral","magenta","blue");

        $ret[3]["title"]    = "Saude Humanos Sorotipo 4";
        $ret[3]["legendas"] = array("Suscetiveis Sorotipo 4","Expostos Sorotipo 4","Infectados Sorotipo 4"," Hemorragicos Sorotipo 4","Imunizados Sorotipo 4","Recuperados Sorotipo 4");
        $ret[3]["cores"]    = array("green","brown","red","LightCoral","magenta","blue");

        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 2 , 361 , 30);
        $ret[1]["valores"][0] = copia_m_v_sum(self::$dados, 3 , 361 , 30);
        $ret[2]["valores"][0] = copia_m_v_sum(self::$dados, 4 , 361 , 30);
        $ret[3]["valores"][0] = copia_m_v_sum(self::$dados, 5 , 361 , 30);

        $ret[0]["valores"][1] = copia_m_v_sum(self::$dados, 7 , 361 , 30);
        $ret[1]["valores"][1] = copia_m_v_sum(self::$dados, 8 , 361 , 30);
        $ret[2]["valores"][1] = copia_m_v_sum(self::$dados, 9 , 361 , 30);
        $ret[3]["valores"][1] = copia_m_v_sum(self::$dados, 10 , 361 , 30);

        $ret[0]["valores"][2] = copia_m_v_sum(self::$dados, 12 , 361 , 30);
        $ret[1]["valores"][2] = copia_m_v_sum(self::$dados, 13 , 361 , 30);
        $ret[2]["valores"][2] = copia_m_v_sum(self::$dados, 14 , 361 , 30);
        $ret[3]["valores"][2] = copia_m_v_sum(self::$dados, 15 , 361 , 30);

        $ret[0]["valores"][3] = copia_m_v_sum(self::$dados, 17 , 361 , 30);
        $ret[1]["valores"][3] = copia_m_v_sum(self::$dados, 18, 361 , 30);
        $ret[2]["valores"][3] = copia_m_v_sum(self::$dados, 19 , 361 , 30);
        $ret[3]["valores"][3] = copia_m_v_sum(self::$dados, 20, 361 , 30);

        $ret[0]["valores"][4] = copia_m_v_sum(self::$dados, 22 , 361 , 30);
        $ret[1]["valores"][4] = copia_m_v_sum(self::$dados, 23 , 361 , 30);
        $ret[2]["valores"][4] = copia_m_v_sum(self::$dados, 24 , 361 , 30);
        $ret[3]["valores"][4] = copia_m_v_sum(self::$dados, 25 , 361 , 30);

        $ret[0]["valores"][5] = copia_m_v_sum(self::$dados, 27 , 361 , 30);
        $ret[1]["valores"][5] = copia_m_v_sum(self::$dados, 28 , 361 , 30);
        $ret[2]["valores"][5] = copia_m_v_sum(self::$dados, 29 , 361 , 30);
        $ret[3]["valores"][3] = copia_m_v_sum(self::$dados, 30 , 361 , 30);

        return $ret;
    }

    function criar_grafico_saude_sexo(){

        $ret;

        $ret[0]["title"]    = "Sexos e Saude Humanos";
        $ret[0]["legendas"] = ["Maculinos Saudaveis","Femininos Saudaveis","Maculinos Expostos","Femininos Expostos","Maculinos Infectados","Femininos Infectados","Maculinos Hemorragicos","Femininos Hemorragicos","Maculinos Imunizados","Femininos Imunizados","Maculinos Recuperados","Femininos Recuperados"];
        $ret[0]["cores"]    = ["green","brown","red","LightCoral","magenta","blue","green","brown","red","LightCoral","magenta","blue"];

        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 1 , 181 , 30);
        $ret[0]["valores"][1] = copia_m_v_sum(self::$dados, 6 , 181 , 30);
        $ret[0]["valores"][2] = copia_m_v_sum(self::$dados, 11 , 181 , 30);
        $ret[0]["valores"][3] = copia_m_v_sum(self::$dados, 16 , 181 , 30);
        $ret[0]["valores"][4] = copia_m_v_sum(self::$dados, 21 , 181 , 30);
        $ret[0]["valores"][5] = copia_m_v_sum(self::$dados, 26 , 181 , 30);

        $ret[0]["valores"][6] = copia_m_v_sum(self::$dados, 181 , 361 , 30);
        $ret[0]["valores"][7] = copia_m_v_sum(self::$dados, 186 , 361 , 30);
        $ret[0]["valores"][8] = copia_m_v_sum(self::$dados, 191 , 361 , 30);
        $ret[0]["valores"][9] = copia_m_v_sum(self::$dados, 196 , 361 , 30);
        $ret[0]["valores"][10] = copia_m_v_sum(self::$dados, 201 , 361 , 30);
        $ret[0]["valores"][11] = copia_m_v_sum(self::$dados, 206 , 361 , 30);

        return $ret;
    }

    function criar_grafico_faixa_etaria_sorotipo(){

        $ret;

        $ret[0]["title"]    = "Sorotipos Humanos Bebes";
        $ret[0]["legendas"] = array("Bebes Infectados Sorotipo 1", "Bebes Infectados Sorotipo 2",
        "Bebes Infectados Sorotipo 3", "Bebes Infectados Sorotipo 4");
        $ret[0]["cores"]    = array("brown","red","LightCoral","magenta");

        $ret[1]["title"]    = "Sorotipos Humanos Criancas";
        $ret[1]["legendas"] = array("Criancas Infectados Sorotipo 1", "Criancas Infectados Sorotipo 2",
        "Criancas Infectados Sorotipo 3", "Criancas Infectados Sorotipo 4");
        $ret[1]["cores"]    = array("brown","red","LightCoral","magenta");

        $ret[2]["title"]    = "Sorotipos Humanos Adolescentes";
        $ret[2]["legendas"] = array("Adolescentes Infectados Sorotipo 1", "Adolescentes Infectados Sorotipo 2","Adolescentes Infectados Sorotipo 3", "Adolescentes Infectados Sorotipo 4");
        $ret[2]["cores"]    = array("brown","red","LightCoral","magenta");

        $ret[3]["title"]    = "Sorotipos Humanos Jovens";
        $ret[3]["legendas"] = array("Jovens Infectados Sorotipo 1", "Jovens Infectados Sorotipo 2",
        "Jovens Infectados Sorotipo 3", "Jovens Infectados Sorotipo 4");
        $ret[3]["cores"]    = array("brown","red","LightCoral","magenta");

        $ret[4]["title"]    = "Sorotipos Humanos Adultos";
        $ret[4]["legendas"] = array("Adultos Infectados Sorotipo 1", "Adultos Infectados Sorotipo 2",
        "Adultos Infectados Sorotipo 3", "Adultos Infectados Sorotipo 4");
        $ret[4]["cores"]    = array("brown","red","LightCoral","magenta");

        $ret[5]["title"]    = "Sorotipos Humanos Idosos";
        $ret[5]["legendas"] = array("Idosos Infectados Sorotipo 1", "Idosos Infectados Sorotipo 2",
        "Idosos Infectados Sorotipo 3", "Idosos Infectados Sorotipo 4");
        $ret[5]["cores"]    = array("brown","red","LightCoral","magenta");


        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 2 , 361 , 180);
        $ret[0]["valores"][1] = copia_m_v_sum(self::$dados, 3 , 361 , 180);
        $ret[0]["valores"][2] = copia_m_v_sum(self::$dados, 4 , 361 , 180);
        $ret[0]["valores"][3] = copia_m_v_sum(self::$dados, 5 , 361 , 180);

        $ret[1]["valores"][0] = copia_m_v_sum(self::$dados, 32 , 361 , 180);
        $ret[1]["valores"][1] = copia_m_v_sum(self::$dados, 33 , 361 , 180);
        $ret[1]["valores"][2] = copia_m_v_sum(self::$dados, 34 , 361 , 180);
        $ret[1]["valores"][3] = copia_m_v_sum(self::$dados, 35 , 361 , 180);

        $ret[2]["valores"][0] = copia_m_v_sum(self::$dados, 62 , 361 , 180);
        $ret[2]["valores"][1] = copia_m_v_sum(self::$dados, 63 , 361 , 180);
        $ret[2]["valores"][2] = copia_m_v_sum(self::$dados, 64 , 361 , 180);
        $ret[2]["valores"][3] = copia_m_v_sum(self::$dados, 65 , 361 , 180);

        $ret[3]["valores"][0] = copia_m_v_sum(self::$dados, 92 , 361 , 180);
        $ret[3]["valores"][1] = copia_m_v_sum(self::$dados, 93 , 361 , 180);
        $ret[3]["valores"][2] = copia_m_v_sum(self::$dados, 94 , 361 , 180);
        $ret[3]["valores"][3] = copia_m_v_sum(self::$dados, 95 , 361 , 180);

        $ret[4]["valores"][0] = copia_m_v_sum(self::$dados, 122 , 361 , 180);
        $ret[4]["valores"][1] = copia_m_v_sum(self::$dados, 123 , 361 , 180);
        $ret[4]["valores"][2] = copia_m_v_sum(self::$dados, 124 , 361 , 180);
        $ret[4]["valores"][3] = copia_m_v_sum(self::$dados, 125 , 361 , 180);

        $ret[5]["valores"][0] = copia_m_v_sum(self::$dados, 152 , 361 , 180);
        $ret[5]["valores"][1] = copia_m_v_sum(self::$dados, 153 , 361 , 180);
        $ret[5]["valores"][2] = copia_m_v_sum(self::$dados, 154 , 361 , 180);
        $ret[5]["valores"][3] = copia_m_v_sum(self::$dados, 155 , 361 , 180);

        return $ret;
    }

    function criar_grafico_faixa_etaria_sexo(){

        $ret;

        $ret[0]["title"]    = "Sexos e Faixas Etarias Humanos";
        $ret[0]["legendas"] = array("Masculinos Bebes","Masculinos Criancas","Masculinos Adolescentes","Masculinos Jovens","Masculinos Adultos","Masculino Idosos","Feminino Bebes","Femininos Criancas","Feminino Adolescentes","Feminino Jovens","Feminino Adultos","Feminino Idosos");
        $ret[0]["cores"]    = array("blue","LightCoral","red","brown","magenta","green");

        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 1   , 31  , 1);
        $ret[0]["valores"][1] = copia_m_v_sum(self::$dados, 31  , 61  , 1);
        $ret[0]["valores"][2] = copia_m_v_sum(self::$dados, 61  , 91  , 1);
        $ret[0]["valores"][3] = copia_m_v_sum(self::$dados, 91  , 121 , 1);
        $ret[0]["valores"][4] = copia_m_v_sum(self::$dados, 121 , 151 , 1);
        $ret[0]["valores"][5] = copia_m_v_sum(self::$dados, 151 , 181 , 1);

        $ret[0]["valores"][6] = copia_m_v_sum(self::$dados, 181 , 211 , 1);
        $ret[0]["valores"][7] = copia_m_v_sum(self::$dados, 211 , 241 , 1);
        $ret[0]["valores"][8] = copia_m_v_sum(self::$dados, 241 , 271 , 1);
        $ret[0]["valores"][9] = copia_m_v_sum(self::$dados, 271 , 301 , 1);
        $ret[0]["valores"][10] = copia_m_v_sum(self::$dados, 301 , 331 , 1);
        $ret[0]["valores"][11] = copia_m_v_sum(self::$dados, 331 , 361 , 1);

        return $ret;
    }

    function criar_grafico_sorotipo_sexo(){

        $ret;

        $ret[0]["title"]    = "Sexos e Sorotipos Humanos";
        $ret[0]["legendas"] = array("Masculinos Suscetiveis","Masculinos Infectados Sorotipo 1","Masculinos Infectados Sorotipo 2","Masculinos Infectados Sorotipo 3","Masculinos Infectados Sorotipo 4","Feminino Suscetiveis","Femininos Infectados Sorotipo 1","Feminino Infectados Sorotipo 2","Feminino Infectados Sorotipo 3","Feminino Infectados Sorotipo 4");
        $ret[0]["cores"]    = array("green","red","LightCoral","magenta","brown");

        $ret[0]["valores"][0] = copia_m_v_sum(self::$dados, 1 , 181 , 5);
        $ret[0]["valores"][1] = copia_m_v_sum(self::$dados, 2 , 181 , 5);
        $ret[0]["valores"][2] = copia_m_v_sum(self::$dados, 3 , 181 , 5);
        $ret[0]["valores"][3] = copia_m_v_sum(self::$dados, 4 , 181 , 5);
        $ret[0]["valores"][4] = copia_m_v_sum(self::$dados, 5 , 181 , 5);

        $ret[0]["valores"][5] = copia_m_v_sum(self::$dados, 181 , 361 , 5);
        $ret[0]["valores"][6] = copia_m_v_sum(self::$dados, 182 , 361 , 5);
        $ret[0]["valores"][7] = copia_m_v_sum(self::$dados, 183 , 361 , 5);
        $ret[0]["valores"][8] = copia_m_v_sum(self::$dados, 184 , 361 , 5);
        $ret[0]["valores"][9] = copia_m_v_sum(self::$dados, 185 , 361 , 5);

        return $ret;
    }
}
?>
