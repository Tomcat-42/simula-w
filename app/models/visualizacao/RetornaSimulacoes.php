<?php
    include_once("../conecta-simula.php");

    $sql = "SELECT
                TABLE_NAME AS TABELA 
            FROM
                INFORMATION_SCHEMA.COLUMNS   
            WHERE
                COLUMN_NAME LIKE 'c0'       
            ORDER BY
                TABELA ASC";

    $query = pg_query($conn_simula,$sql);
    $ntables = pg_numrows($query);
    
    for( $i = 0 ; $i < $ntables ; $i++){
        $tabelas[] = pg_fetch_assoc($query, $i);
    }
    echo json_encode($tabelas, JSON_PRETTY_PRINT);
?>