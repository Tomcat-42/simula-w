<?php
    header("Content-Type: text/plain");
    include_once("individualFuncoes.php");
    include_once("../conecta-simula.php");

    $agente = $_POST['agente'];
    $atributos = retornaAtributos($agente, $conn_simula, false); 
    echo json_encode($atributos, JSON_PRETTY_PRINT);
?>