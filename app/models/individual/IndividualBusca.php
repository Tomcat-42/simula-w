<?php
    header("Content-Type: text/plain");

    $ciclo    = $_POST['ciclo'];
    $agente    = $_POST['agente'];
    $filtros   = $_POST['filtros'];
    $atributos = $_POST['atributos'];

    $stringAgente;
    $stringAtributos = [];
    $stringFiltros = [];

    if($agente == "Humanos")
        $stringAgente = 'h';
    else
        $stringAgente = 'm';

    foreach($atributos as &$atributo){
       $stringAtributos[] = $atributo['pref'];
    }
    
    if($filtros == null)
        $filtros = [];

    $jsonResp = [];

    $jsonResp['ciclo'] = $ciclo;
    $jsonResp['agente'] = $stringAgente;
    $jsonResp['filtros'] = $filtros;
    $jsonResp['atributos'] = $stringAtributos;
    $json_busca = json_encode($jsonResp, JSON_PRETTY_PRINT);

    $resultado;

    try{
        $fp = fopen("../../../apk/acompanhamento_Individual/entradaBusca.json", "w");
        $ff = fopen("../../../apk/acompanhamento_Individual/saidaBusca.json", "w");
        $escreve = fwrite($fp, $json_busca);
        fclose($fp);
        fclose($ff);
        $resultado = "sucesso";
    }catch(Exception $e){
        echo json_encode("erro");
        $resultado = "erro";
    }
    if($resultado != "erro"){
        $execut = "../../../apk/acompanhamento_Individual/saidasbitstring/Busca.py";
        $comando   = escapeshellcmd('python3 '.$execut);
        $cmdResult = exec($comando);
        $arquivo = file_get_contents('../../../apk/acompanhamento_Individual/saidaBusca.json');
        echo $arquivo;
    }
?>