<?php
/*
*   Matheus N Ismael 19/03/19
*
*/
include_once('TabelasEntradasSimulacao.php');
include_once('TabelaEntradasAmbientais.php');
include_once('../conecta.php');
include_once('../conecta-simula.php');

$HumanosDados   = $_POST['humanos'];
$MosquitosDados = $_POST['mosquitos'];
$SimulacaoDados = $_POST['simulacao'];
$simul           = $_POST['simul'];

$HumanosTabelas   = [];
$MosquitosTabelas = [];
$SimulacaoTabelas = [];

$HumanosTabelas   = json_decode($HumanosDados, true);
$MosquitosTabelas = json_decode($MosquitosDados, true);
$SimulacaoTabelas = json_decode($SimulacaoDados, true);

$Resultados = [];

if($HumanosTabelas != [] && $MosquitosTabelas != [] && $SimulacaoTabelas != []){
    $sqlLimpaBase = "TRUNCATE TABLE ".$simul;
    $queryLimpaBase = pg_query($conn_simula, $sqlLimpaBase);

    $tabelaMosquitos = new TabelaEntradasSimulacao("mosquitos", $simul, $conn_simula, $conn_simula, $MosquitosTabelas);
    $Resultados["Mosquitos"] = $tabelaMosquitos->{"salvaTabela"}();

    $tabelaHumanos   = new TabelaEntradasSimulacao("humanos",   $simul, $conn_simula, $conn_simula, $HumanosTabelas);
    $Resultados["Humanos"]   = $tabelaHumanos->{"salvaTabela"}();

    $tabelaSimulacao = new TabelaEntradasSimulacao("simulacao", $simul, $conn_simula, $conn_simula, $SimulacaoTabelas);
    $Resultados["Simulacao"] = $tabelaSimulacao->{"salvaTabela"}();
}

echo json_encode($Resultados, JSON_PRETTY_PRINT);
?>