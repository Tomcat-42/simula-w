<?php
    include_once('SimulacaoFuncoes.php');
    include_once("../conecta.php");

    $resp = false;
    try{
        $emExec = retornaEmExecucao($conn);
        if($emExec != null)
            $resp = true;
    }catch(Esception $e){
        $resp = false;
    }

    echo json_encode($resp);
?>