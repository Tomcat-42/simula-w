<?php
/*
*   Matheus N Ismael 28/02/2019
*
*/

class TabelaEntradasAmbientais{
    private static $simulacaoBase;
    private static $conn;
    private static $tabela;
    private static $dados;

    public function __construct($simulacaoBase, $conn, $dados){
            self::$simulacaoBase = $simulacaoBase;
            self::$conn          = $conn;
            self::$tabela        = [];
            self::$tabela = $this->obtemDados();
            self::$dados = $dados;
    }
    
    private function obtemDados(){
       $dados = [];

       $sqlTabela   = "SELECT * FROM ".self::$simulacaoBase."_ambientais ORDER BY id";
       $queryTabela = pg_query(self::$conn, $sqlTabela);

        if($nlinhas = pg_numrows($queryTabela))
        for($i = 0 ; $i < $nlinhas ; $i++)
           $dados[] = pg_fetch_array($queryTabela, $i);
        return $dados;
    }
    
    public function salvaTabela(){
        $success = true;
        
        $sqlLimpaDados = "TRUNCATE TABLE ".self::$simulacaoBase."_ambientais";
        pg_query(self::$conn, $sqlLimpaDados);
        
        for($i = 0 ; $i < sizeof(self::$dados) ; $i++ ){
            $regAtual = self::$dados[$i];
            $sqlSave = "INSERT INTO ".self::$simulacaoBase."_ambientais VALUES ('".$regAtual['id']."','".$regAtual['key']."','".$regAtual['value']."');";
            $querySave = pg_query(self::$conn, $sqlSave);
            if(!$querySave)
                $success = false;
        } 
        if($success)
            return "salvo";
        return "erro";       
    }

    public function retornaTabela(){
        return (json_encode(self::$tabela, JSON_PRETTY_PRINT));
    } 
}
?>