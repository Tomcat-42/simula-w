<?php
/*
*   Matheus N Ismael 17/03/19
*
*/
    include_once('TabelasEntradasSimulacao.php');
    include_once('TabelaEntradasAmbientais.php');
    include_once('../conecta.php');
    include_once('../conecta-simula.php');
    
    $dados = $_POST['dados'];
    $simul  = $_POST['base'];

    $tabela = new TabelaEntradasAmbientais($simul, $conn_simula, $dados);

    $Tabela = $tabela->{"salvaTabela"}();

    echo json_encode($Tabela, JSON_PRETTY_PRINT);
?>