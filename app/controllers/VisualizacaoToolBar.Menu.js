///////////////////////carrega lista de layers do banco\\\\\\\\\\\\\\\\\\\\\\\
var listaVisualizacao = document.getElementById("mapas");
function LoadMapsFromDb(lista) {
    $.ajax({
        type: "POST",
        url: "../app/models/visualizacao/LoadTables.php",
        dataType: 'json',
        contentType: 'application/json',
        crossDomain: true,
        cache: false,
        success: function(data) {
            let conteudoJSON = data;

            if (data[0].erro) {
                alert(data[0].erro);
            } else {
                $(lista).empty();
                let opt = document.createElement('option');
                opt.value = "--";
                opt.innerHTML = "--";
                lista.appendChild(opt);
                for (var i = 0; i < data.length; i++) {
                    let opt = document.createElement('option');
                    opt.value = data[i].tablename;
                    opt.innerHTML = data[i].tablename;
                    lista.appendChild(opt);
                }
            }
        },
        error: function(errorThrown) {
            alert('Erro ao carregar');
            console.log(errorThrown);
        }
    });
}
///////////////////////carregamento lista de simulações do banco\\\\\\\\\\\\\\\\\\\\\\\
function loadselectSimulacoes() {
    var select = document.getElementById("simulacoes");
   $.ajax({
       type: "POST",
       url: "../app/models/visualizacao/RetornaSimulacoes.php",
       dataType: 'json',
       contentType: 'application/json',
       crossDomain: true,
       cache: false,
       success: function(data) {
           let conteudoJSON = data;

           if (data[0].erro) {
               alert(data[0].erro);
           } else {
               for (var i = 0; i < data.length; i++) {
                   let opt = document.createElement('option');
                   opt.value = data[i].tabela;
                   opt.innerHTML = data[i].tabela;
                   select.appendChild(opt);
               }
           }
       },
       error: function(errorThrown) {
           alert('Erro ao carregar');
           console.log(errorThrown);
       }
   });
}

///////////////////////Filtros Simulação\\\\\\\\\\\\\\\\\\\\\\\


function loadIndividuos(){//carrega em memoria os codigos e nomes dos individuos
    var select = document.getElementById("selectIndividuos");
    $.ajax({
        type: "POST",
        url: "../app/models/visualizacao/LoadTabelaIcones.php",
        dataType: 'json',
        contentType: 'application/json',
        crossDomain: true,
        cache: false,
        success: function(data) {
            let conteudoJSON = data;

            if (data[0].erro) {
                alert(data[0].erro);
            } else {
                for (var i = 0; i < data.length; i++) {
                    individuos[i] = new Object();
                    individuos[i].cod = data[i].cod;
                    individuos[i].nome = data[i].nome;
                    individuos[i].visivel = null;
                }
                loadSelectIndividuos();
            }
        },
        error: function(errorThrown) {
            alert('Erro ao carregar');
            console.log(errorThrown);
        }
    });
}

function loadSelectIndividuosSelecionados(){///preenche o select com os individuos selecionados
    $("#individuosSelecionados").empty();
    var select = document.getElementById("individuosSelecionados"); 

    for(var i = 0 ; i < individuosSelecionados.length ; i++){
        let opt = document.createElement('option');
        opt.value = individuosSelecionados[i].cod;

        opt.title = individuosSelecionados[i].nome;

        opt.innerHTML = individuosSelecionados[i].cod + "</br>" + individuosSelecionados[i].nome;
        $(opt).addClass("stIndividuo");
        opt.style.backgroundImage = 'url(../../imagens/icones/'+individuosSelecionados[i].cod+'.png)';

        criaEventoRemoveIndividuo(opt,i);

        select.appendChild(opt);
    }
}

function loadSelectIndividuos(){//preenche o select que mostra todos os individuos
    var select = document.getElementById("selectIndividuos"); 
    
    for(var i = 0 ; i < individuos.length ; i++){
        var nome = individuos[i].nome;
        var cod  = individuos[i].cod;

        let opt = document.createElement('option');
        opt.value = cod;

        opt.title = nome;

        opt.innerHTML = cod + " " + nome;
        $(opt).addClass("stIndividuo");
        opt.style.backgroundImage = 'url(../../imagens/icones/'+cod+'.png)';

        criaEventoAdicionaIndividuo(opt,cod,nome);
        
        select.appendChild(opt);
    }
}

function criaEventoAdicionaIndividuo( opt , cod , nome ){
    opt.addEventListener('click', function() {
        if(!setIndividuosSelecionados.has(cod)){
            var ind = individuosSelecionados.length;
            individuosSelecionados[ind] = new Object();
            individuosSelecionados[ind].cod  = cod;
            individuosSelecionados[ind].nome = nome;

            setIndividuosSelecionados.add(cod);

            loadSelectIndividuosSelecionados();
        }
    });
}

function criaEventoRemoveIndividuo( opt , i ){
    opt.addEventListener('click', function() {
        setIndividuosSelecionados.delete(individuosSelecionados[i].cod);
        individuosSelecionados.splice(i, 1);
        loadSelectIndividuosSelecionados();
    });
}

////////////////////////////////////////EDITAR/////s


function tiraPrint(){
    map.once('rendercomplete', function(event) {
      var zip = new JSZip();
      var canvas = event.context.canvas;
        for(var i = 1 ; i < 30 ; i++){  
          cicloAtualInput.value++;
          atualizaCiclo();
  
          console.log("aqui");
      
          if(i == 29){
            console.log("ASQSqS");
            if (navigator.msSaveBlob) {
              navigator.msSaveBlob(canvas.msToBlob(), 'map.png');
            } else {
              canvas.toBlob(function(blob) {
                saveAs(blob, 'map.png');
              });
            }
          }
        }
    });
    map.renderSync();
  };




















