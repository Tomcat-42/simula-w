
//indice do mapinha sendo editado
var indiceEditado;

//inputs de edição do mapinhaa
var colorStroke  = document.getElementById("colorStroke");
var sizeStroke  = document.getElementById("sizeStroke");

var colorFill  = document.getElementById("colorFill");
var opaciFill = document.getElementById("opaciFill");

//variavel para sincronizaçao do carregamento do mini-mapa com o carregamento da modal
var loaded = false;

//mapa
var mapinha;

//figura de demonstração da edição de estilo
var geojsonObject = {
    'type': 'FeatureCollection',
    'crs': {
        'type': 'name',
        'properties': {
        'name': 'EPSG:3857'
        }
    },
    'features': 
    [{
        'type': 'Feature',
        'geometry': {
        'type': 'Polygon',
        'coordinates': [[[-2e6, -1e6], [-1e6, 1e6],
            [0, -1e6], [-2e6, -1e6]]]
        }
    }]
};

//estilo para carregamento da figura
var styleAtual = new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: '#ef1818',
            width: 3
        }),
        fill: new ol.style.Fill({
            color: 'rgba(0, 0, 255, 0.1)'
        })
    })

var source = new ol.source.Vector({
        features: (new ol.format.GeoJSON()).readFeatures(geojsonObject)
    });

var layer = new ol.layer.Vector({
    source: source,
    style: styleAtual
});

function loadMiniMap(){
    var mapinha = new ol.Map({
        layers: [],
        target: 'mapinha',
        view: new ol.View({
        center: [-2000, -1000],
        zoom: 4
        }),
        interactions: ol.interaction.defaults({
            doubleClickZoom :false,
            dragAndDrop: false,
            keyboardPan: false,
            keyboardZoom: false,
            mouseWheelZoom: false,
            pointer: false,
            select: false,
            dragPan: false
        }),
        controls: []
    });
    mapinha.addLayer(layer);
    return mapinha;
}

//carrega o minimapa dentro da modal, quando ela é acionada
$('#styleEditaMapas').on('shown.bs.modal', function () {
    if(!loaded){
        mapinha = loadMiniMap();
        loaded = true;
    }
    atStyle();
})

function carregaEstilo(FillCoratual, StrokeCor , StrokeSize , indice){
    FillCoratual = tinycolor(FillCoratual);
    StrokeCor = tinycolor(StrokeCor);
    
    colorStroke.value = "#"+StrokeCor.toHex()
    sizeStroke.value  = StrokeSize;

    colorFill.value   = "#"+FillCoratual.toHex();
    opaciFill.value   = FillCoratual.getAlpha()*10;

    indiceEditado = indice;
}

function atStyle(){
    mapinha.setLayerGroup(new ol.layer.Group());
      
      var ncolorStroke = tinycolor(colorStroke.value);

      var ncolorFill = tinycolor(colorFill.value);
      ncolorFill.setAlpha(opaciFill.value/10);

      var nst = new ol.style.Style({
        stroke: new ol.style.Stroke({
          color: "#"+ncolorStroke.toHex(),
          width: sizeStroke.value
        }),
        fill: new ol.style.Fill({
          color: ncolorFill.toRgbString()
        })
      })

      var source = new ol.source.Vector({
        features: (new ol.format.GeoJSON()).readFeatures(geojsonObject)
      });

      var layer = new ol.layer.Vector({
        source: source,
        style: nst
      });

      mapinha.addLayer(layer);
}

function salvaStyle(){
    var ncolorStroke = tinycolor(colorStroke.value);

    var ncolorFill = tinycolor(colorFill.value);
    ncolorFill.setAlpha(opaciFill.value/10);
    
    var NStyle = new ol.style.Style({
        stroke: new ol.style.Stroke({
          color: "#"+ncolorStroke.toHex(),
          width: sizeStroke.value
        }),
        fill: new ol.style.Fill({
          color: ncolorFill.toRgbString()
        })
      })

      if(!editPol){
        camadas[visivel[indiceEditado]['indexCamadas']].setStyle(NStyle);
        atualizaMapa();
      }
      else{
        alteraSelecionados(NStyle);
        editPol = false;
      }
}

//=============================================================== Seleciona poligonos

var editarSelecionados = document.getElementById("editarSelecionados");
var reescalonarIcones  = document.getElementById("reescalonarIcones");
var editPol = false;

//seleção de um poligono
var select = new ol.interaction.Select();
map.addInteraction(select);
var selectedFeatures = select.getFeatures();
//

//box seleção
var dragBox = new ol.interaction.DragBox({
  condition: ol.events.condition.platformModifierKeyOnly
});
map.addInteraction(dragBox);
dragBox.on('boxend', function() {
  var extent = dragBox.getGeometry().getExtent();
  map.getLayerGroup().forEachFeatureIntersectingExtent(extent, function(feature) {
    selectedFeatures.push(feature);
  });
});
//

var styleTST = new ol.style.Style({
  stroke: new ol.style.Stroke({
      color: '#ef1818',
      width: 3
  }),
  fill: new ol.style.Fill({
      color: 'rgba(229, 35, 35, 0.1)'
  })
})  

function alteraSelecionados(style){
  let nSelec = selectedFeatures.getArray().length;

  for(let i = 0; i < nSelec; i++){
    selectedFeatures.getArray()[i].setStyle(style);
  }
}

editarSelecionados.addEventListener("click", function(){
  editPol = true;
  $('#styleEditaMapas').modal('show');
  hideContextMenu();
})

//=============================================================== Context Menu

var contextMenu = document.getElementById("contextMenu");

var mapa = document.getElementById("mapa");

mapa.addEventListener("contextmenu", function(e){
    e.preventDefault();
    $("#contextMenu").toggleClass("contextMenu-hide");
    $("#contextMenu").css(
        {
            position : "absolute",
            top: e.pageY,
            left: e.pageX,
            display: "block"
        }
    )
    console.log(e);
}, false);

mapa.addEventListener("click", function(e){
    hideContextMenu();
}, false);

function hideContextMenu(){
    $("#contextMenu").css(
        {
            display: "none"
        }
    )
}