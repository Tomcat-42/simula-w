var nomeSimulacao = document.getElementById("nomeNovaSimulacao");
var simulacaoNova = [];
var tituloNova = document.getElementById("tituloNova");
var statusSimNova = document.getElementById("statusSimNova");
var ambienteNova = document.getElementById("ambienteNova");
var doencaNova = document.getElementById("doencaNova");
var duracaoNova = document.getElementById("duracaoNova");
var infosSimulacaoNova = document.getElementById("infosSimulacaoNova");
var boxSalva           = document.getElementById("box-salva");
var boxExecuta         = document.getElementById("boxExecuta");

//================================================================

function simulacaoBase(){
    for(var  i = 0 ; i < Simulacoes.length ;i++)
        if(Simulacoes[i]['nome'] == simulaBase.value)
            return Simulacoes[i];
     return null;   
}

function carregaInfosNova(){
    var nome = nomeSimulacao.value;
    var base = simulacaoBase();

    simulacaoNova['nome']       = nome;
    simulacaoNova['status']     = "naoProcessado";
    simulacaoNova['ambiente']   = base['ambiente'];
    simulacaoNova['doenca']     = base['doenca'];
    simulacaoNova['duracaoano'] = base['duracaoano'];
}

function atualizaMenuNova(){
    infosSimulacaoNova.style.display = 'block';
    var atual = simulacaoBase();
    tituloNova.innerHTML = atual['nome'].toUpperCase();
    statusSimNova.innerHTML = atual['status'].toUpperCase();
    ambienteNova.innerHTML = atual['ambiente'].toUpperCase();
    doencaNova.innerHTML = atual['doenca'].toUpperCase();
    duracaoNova.innerHTML = atual['duracaoano'];
}

function criaRegistroNova(){
    simulacao = new Object();

    simulacao.nome       = simulacaoNova['nome'];
    simulacao.status     = simulacaoNova['status'];
    simulacao.ambiente   = ConteudoAmbientais[0]['value'];
    simulacao.doenca     = ConteudoAmbientais[1]['value'];
    simulacao.duracaoano = ConteudoAmbientais[14]['value'];

    var Simulacao   = JSON.stringify(simulacao);

    $.ajax({
        url : "../app/models/simulacao/CriaTabelaSimulacao.php",
        type : "POST",
        dataType : "json",
        async : false,
        data : {'simulacao' : simulacao},
        success : function(data){
            //REMOVER
            console.log(data);
        },
        error : function(data){
            
        }
    });
}

function criaTabelaRegistros(){
    $.ajax({
        url : "../app/models/simulacao/CriaTabela.php",
        type : "POST",
        dataType : "json",
        async : false,
        data : {'simulacao' : simulacaoNova['nome']},
        success : function(data){
            console.log(data);
        },
        error : function(data){
            
        }
    });
}

function criaTabelaAmbientais(){
    $.ajax({
        url : "../app/models/simulacao/CriaTabelaAmbientais.php",
        type : "POST",
        dataType : "json",
        async : false,
        data : {'simulacao' : simulacaoNova['nome']},
        success : function(data){
            console.log(data);
        },
        error : function(data){
            
        }
    });
}

function salvaNova(){
    criaTabelaRegistros();
    criaTabelaAmbientais();
    criaRegistroNova();
    salvaArquivosTabelas(simulacaoNova['nome']);
    salvarAmbientais(simulacaoNova['nome']);
    resetVisualizacao();
}

function SalvaAlteracoes(){
    salvarAmbientais(inputSimulacao.value);
    salvaArquivosTabelas(inputSimulacao.value);
}

function excluiRegistros(){
    $.ajax({
        url: "../app/models/simulacao/ExcluiTabelaParametros.php",
        type: "POST",
        dataType: "json",
        data : {'simulacao' : inputSimulacao.value},
        success : function(data){
            if(data == "success")
                alerta("Tabelas Excluidas", "Simulação. Todas as tabelas de parametros foram excluidas");
            else
                alerta("Erro", "Simulação. Não foi possivel remover completamente os registros desta tabela, ou estes estavam incompletos");

            resetVisualizacao()
        },
        error: function(data){
            alerta("Erro", "Simulação. Não foi possivel remover os registros desta tabela, houve um erro interno");
            resetVisualizacao()
        }
    });
}

// Modais
function exibeMenu(exibe){
    if(exibe)
        boxSalva.style.display = "block";
    else
        boxSalva.style.display = "none";
}
function exibeExecuta(exibe){
    if(exibe)
        boxExecuta.style.display = "block";
    else
        boxExecuta.style.display = "none";
}

//Eventos
btnSalvaNovaSimulacao = document.getElementById("btnSalvaNovaSimulacao");
btnSalvaNovaSimulacao.addEventListener("click",salvaNova);

btnSalvaAlteracoes = document.getElementById("btnSalvaAlteracoes");
btnSalvaAlteracoes.addEventListener('click',SalvaAlteracoes);

btnExcluiSimulacao = document.getElementById("btnExcluiSimulacao");
btnExcluiSimulacao.addEventListener('click', excluiRegistros);

simulaBase.addEventListener('change',atualizaMenuNova);