var divAvancoSimul = document.getElementById("divAvancoSimul");
var btnAtualizaAvanco = document.getElementById("btnAtualizaAvanco");

//============================================================

function carregaAvancoSimulacoes(){
    $.ajax({
        type : "POST",
        url : "../app/models/simulacao/RetornaInfosSimulacoes.php",
        dataType: "json",
        success: function(data){
            Simulacoes = data;
            $(divAvancoSimul).empty();
            for(var  i = 0 ; i < Simulacoes.length ; i++){
                var boxSimulA = document.createElement("div");
                    $(boxSimulA).addClass("SimulA");
                    boxSimulA.innerHTML = Simulacoes[i]['nome'];
                    var statusSim = document.createElement("span");
                        statusSim.innerHTML = Simulacoes[i]['status'];
                        $(statusSim).addClass("StatusSimulAt");
                    boxSimulA.appendChild(statusSim);
                
                    var barraProgresso = document.createElement("div");
                    $(barraProgresso).addClass("progress");
                        var progresso = document.createElement("div");
                            $(progresso).addClass("progress-bar bg-success");
                            progresso.style.width = Simulacoes[i]['avanco']+"%";
                            progresso.innerHTML = Simulacoes[i]['avanco']+"%";
                        barraProgresso.appendChild(progresso);
                    boxSimulA.appendChild(barraProgresso);
                divAvancoSimul.appendChild(boxSimulA);
            }
        },
        error: function(){
            alert("Erro", 'Simulação. Erro ao Carregar!');
            console.log(errorThrown);
         }
    });
} 

btnAtualizaAvanco.onclick = carregaAvancoSimulacoes;

carregaAvancoSimulacoes();