/*
    Matheus N Ismael 20/02/19
*/
$("#ambiente-tab").load("tabelasAmbiente.html");
$("#humanos-tab").load("tabelasHumanos.html");
$("#mosquitos-tab").load("tabelasMosquitos.html");
$("#simulacao-tab").load("tabelasSimulacao.html");
carregaSelectSimulacoes(inputSimulacao);
carregaSelectSimulacoes(simulaBase);