//////////////////////ESTILOS\\\\\\\\\\\\\\\\\\\\

var defaultStyle = new ol.style.Style({
    fill: new ol.style.Fill({
        color: 'rgba(255, 255, 255, 0.6)'
    }),
    stroke: new ol.style.Stroke({
        color: '#0000ff',
        width: 1
    })
});

var defaultS = new ol.style.Style({
    fill: new ol.style.Fill({
        color: 'rgba(255, 255, 255, 0.6)'
    }),
    stroke: new ol.style.Stroke({
        color: '#0000ff',
        width: 1
    })
});

//////////////////////CAMADAS\\\\\\\\\\\\\\\\\\\\
var center = ol.proj.transform([-53.46330958485, -24.9614305105536], 'EPSG:4326', 'EPSG:3857');

//vetores de layers
var camadas = []; // vetor que guarda os mapas em formato layer nativo do OpenLayers
var visivel = []; // vetor que guarda as informações de cada mapa no vetor de mapas (campos descritos na linha 133)

var osmaps = false; // flag que sinaliza a adição do OSM a cada novo carregamento do mapa
//layer OpenStreetMaps
var OSM = new ol.layer.Tile({
    source: new ol.source.OSM({ layer: 'osm' }),
    preload: 2
});
OSM.set('name', 'OSM');

var idLayers = 0;

//////////////////////DEFINIÇÃO DA VIEW DO MAPA\\\\\\\\\\\\\\\\\\\\

//Ver posição do cursor
var mousePositionControl = new ol.control.MousePosition({
    coordinateFormat: ol.coordinate.createStringXY(4),
    projection: 'EPSG:4326',
    className: 'custom-mouse-position',
    target: document.getElementById('mouse-position'),
    undefinedHTML: '&nbsp;'
});

var view = new ol.View({
    center: center,
    zoom: 19
});

var map = new ol.Map({
    controls: ol.control.defaults({
          attributionOptions: {
            collapsible: false
          }
        }).extend([mousePositionControl]),
    layers: camadas,
    target: 'map',
    view: view
});

//////////////////////FUNÇÕES\\\\\\\\\\\\\\\\\\\\
function atualizaMapa() {
    map.setLayerGroup(new ol.layer.Group());
    if (osmaps)
        map.addLayer(OSM);
    for (var i = 0; i < camadas.length; i++)
        if (visivel[i]['visivel'])
            map.addLayer(camadas[visivel[i]['indexCamadas']]);
}

//////////////////////LAYERS\\\\\\\\\\\\\\\\\\\\
function AdcLayer() {
    var camada = document.getElementById("mapas").value;
    toggleLoaderMap(true);
    $.ajax({
        type: "POST",
        url: "../app/models/visualizacao/mapas/VerificaTipoLayer.php",
        dataType: "json",
        data: { layer: camada },
        success: function(data) {
            if (data[0] == undefined) {
                alerta("Erro", "Visualização. Não foi possivel obter o shape");
            } else {
                if (data == "POLIGONO")
                    CarregaMapa(camada, map, camadas, true, false, null);
                else
                    CarregaPontos(camada, map, camadas, true);
                    toggleLoaderMap(false);
            }
        },
        error: function(data) {
            alerta("Erro", 'Visualização. Erro ao instanciar a layer : ' + data);
        }
    });
}

function CarregaPontos(camada, map, camadas, layerStack) {
    $.ajax({
        type: "POST",
        url: "../app/models/visualizacao/mapas/VerificaPontoMapa.php",
        dataType: "json",
        data: { layer: camada },
        success: function(data) {
            if (data[0].erro) {
                alert(data.erro);
            } else {
                var vectorSource = new ol.source.Vector();
                var centerShape  = new Object();
                let id = idLayers++;
                for (var i = 0; i < data.length; i++) {
                    ponto = data[i].regexp_replace.split(' ');
                    var iconFeature = new ol.Feature(new ol.geom.Point([ponto[0], ponto[1]]));
                    vectorSource.addFeature(iconFeature);
                    centerShape.lat = ponto[0];
                    centerShape.lng = ponto[1];
                }

                var st = createStyle();
                var vectorLayerPontos = new ol.layer.Vector({ source: vectorSource, style: st });
                vectorLayerPontos.set('name', id);
                
                camadas.push(vectorLayerPontos);
                map.addLayer(vectorLayerPontos);

                visivel.push({ // a ordem neste vetor indica a ordem de sobreposição na visualização
                    id : id, // identificador unico da layer
                    indexCamadas : camadas.length - 1, // indice da layer no vetor de layers
                    visivel : true, // indica se a leyer será carregada na visualização
                    nome : camada, // nome da layer exibido la stack de visualização
                    tipo : "pontos", // tipo de layer pode variar entre "pontos", "mapa" e "simulacao"
                    centro : [centerShape.lat , centerShape.lng], // guarda um ponto pertencente a layer para aproximar a visualização a ela
                });

                view.centerOn([centerShape.lat , centerShape.lng], map.getSize(), [500, 200]);
                if(layerStack)
                    AdicionaIconeMapa(id, camada, "pontos");
            }
        },
        error: function(jqXHR, textStatus, errorThrown, data) {
            alerta("Erro", 'Visualização. Erro ao Carregar conjunto de pontos');
            console.log(errorThrown);
            console.log(data);
        }
    });
}

function CarregaMapa(camada, map, camadas, layerStack, lira, nomeCustom) {
    $.ajax({
        type: "POST",
        url: "../app/models/visualizacao/mapas/VerificaPontoMapa.php",
        dataType: "json",
        data: { layer: camada },
        success: function(data) {
            let defaultStyle = new ol.style.Style({
                fill: new ol.style.Fill({
                    color: 'rgba(255, 255, 255, 0.2)'
                }),
                stroke: new ol.style.Stroke({
                    color: '#0000ff',
                    width: 1
                })
            });

            if (data[0] == undefined) {
                alerta("Erro", "Visualização. Erro ao obter gdados do mapa");
            } else {
                let vectorSource = new ol.source.Vector();
                let nomeLayer;
                let id = idLayers++;
                let vectorLayerMapa;

                if(nomeCustom == null)
                    nomeLayer = camada;
                else 
                    nomeLayer = nomeCustom;
                for(let j = 0; j < data.length; j++) {
                    let latlong = [];
                    let geos = [];
                    latlong = data[j]['regexp_replace'].split(',');
                    let content = [];
                    for (let i = 0, c = 0; i < latlong.length; i++) {
                        let ponto = [];
                        ponto = latlong[i].split(' ');
                        geos[c] = new Object();
                        geos[c].lng = ponto[0];
                        geos[c].lat = ponto[1];
                        if(content[geos[c].lng+"a"+geos[c].lat]){
                            vectorSource.addFeature(createFeature(geos, lira, data[j]['quadra']));
                            geos = [];
                            content = [];
                            c = 0;
                        }
                        else{
                            content[geos[c].lng+"a"+geos[c].lat] = true;
                            c++;
                        }  
                    }    
                }
                if(lira)
                    vectorLayerMapa = new ol.layer.Vector({ source: vectorSource});
                else
                    vectorLayerMapa = new ol.layer.Vector({ source: vectorSource, style: defaultStyle });

                vectorLayerMapa.set('name', id);
                if(layerStack)
                    AdicionaIconeMapa(id, nomeLayer,false);

                map.addLayer(vectorLayerMapa);
                camadas.push(vectorLayerMapa);
                
                visivel.push({
                    id : id,
                    indexCamadas : camadas.length - 1,
                    visivel : true,
                    nome : nomeLayer,
                    tipo : "mapa",
                    centro : [data[0].st_x , data[0].st_y],
                });

                view.centerOn([data[0].st_x , data[0].st_y], map.getSize(), [500, 200]);
            }
        },
        error: function(jqXHR, textStatus, errorThrown, data) {
            alerta("Erro",'Visualização. Erro ao carregar mapa');
            console.log(errorThrown);
            console.log(data);
        }
    });
}

function createFeature(geos, lira, infquadra){

    let defaultStyle2 = new ol.style.Style({
        fill: new ol.style.Fill({
            color: 'rgba(230, 230, 230, 0.7)'
        }),
        stroke: new ol.style.Stroke({
            color: '#000000',
            width: 0.5
        })
    });

    let b = geos.map(function(item) {
        return [item.lng, item.lat];
    });
    
    let polygon = new ol.geom.Polygon([b]);
    let feature = new ol.Feature(polygon);

    if(lira){
        if(infquadra != '0000'){
            let cor;
            let estado = liraaAtualQuadrasMap[infquadra];
            if(estado == "Satisfatorio")
                cor = liraaCorSatisfatorio.value;
            else if(estado == "Alerta")
                cor = liraaCorAlerta.value;                        
            else
                cor = liraaCorRisco.value;

            let estiloPol = createStyleColor(cor);
            feature.setStyle(estiloPol);
        }
        else
            feature.setStyle(defaultStyle2);
    }
    return feature;
}

function AdicionaOSM() {
    osmaps = !osmaps;
    atualizaMapa();
}

function removeShapes(){
    camadasRastrear = [];
    atualizaMapa();
}

function createStyle() {
    var sty =
        new ol.style.Style({
            image: new ol.style.Circle({
                radius: 5,
                fill: new ol.style.Fill({ color: '#3399ff' }),
                stroke: new ol.style.Stroke({ color: '#00000', width: 0.5 })
            })
        });
    return sty;
}

function createStyleColor(cor){
    let cor_rgba = tinycolor(cor);
    cor_rgba.setAlpha(1);
    console.log(cor_rgba.toRgbString());
    var sty = new ol.style.Style({
            fill: new ol.style.Fill({
                color: cor_rgba.toRgbString()
            }),
            stroke: new ol.style.Stroke({
                color: '#000000',
                width: 0.5
            })
        });
    return sty;
}