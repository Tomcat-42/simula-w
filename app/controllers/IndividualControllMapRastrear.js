//============================================================ flags de visualização dos pontos
var mantemPontosRastrear = false; // destaca o ponto atual mas mantem as posições anteriores na visualização
var tracarCaminho        = false; // traça o caminho seguido pelo agente
var zoomPosAtual         = false; // desloca o enquadramento para a posição atual a cada avanço
var osmRastrear          = false; // adicionar layer Open Street Maps de fundo
var stopAnim             = false; // se verdadeiro para o andamento da animação
//============================================================ componentes
var btnOpcoesRastreamento   = document.getElementById("btnOpcoesRastreamento");
var btnMantemPontosRastrear = document.getElementById("btnMantemPontosRastrear");
var btnTracarCaminho        = document.getElementById("btnTracarCaminho");
var btnZoomPosAtual         = document.getElementById("btnZoomPosAtual");
var btnOsmRastrear          = document.getElementById("btnOsmRastrear");
var inputJumpCiclo          = document.getElementById("inputJumpCiclo");
var btnIrJumpCiclo          = document.getElementById("btnIrJumpCiclo");
var btnPlayRastreamento     = document.getElementById("btnPlayRastreamento");
var btnPauseRastreamento    = document.getElementById("btnPauseRastreamento");

var mapasRastrear = document.getElementById("mapasRastrear");
//============================================================ mapa
var center = ol.proj.transform([-53.46330958485, -24.9614305105536], 'EPSG:4326', 'EPSG:3857');

var ciclosVisiveis = []

var camadasRastrear = [];

var viewRastrear = new ol.View({
    center: center,
    zoom: 19
});

var mapRastrear = null;

//============================================================

function atualizaValoresVisualizacao(){
    mantemPontosRastrear = btnMantemPontosRastrear.checked;
    tracarCaminho        = btnTracarCaminho.checked;
    zoomPosAtual         = btnZoomPosAtual.checked;
    osmRastrear          = btnOsmRastrear.checked;
}

function createSt() {
    var sty = new ol.style.Style({
            image: new ol.style.Circle({
                radius: 7,
                fill: new ol.style.Fill({ color: '#666666' }),
                stroke: new ol.style.Stroke({ color: '#bada55', width: 0.5 })
            })
        });
    return sty;
}

function atualizaMapaRastrear() {
    mapRastrear.setLayerGroup(new ol.layer.Group());
    if(osmRastrear)
        mapRastrear.addLayer(new ol.layer.Tile({source: new ol.source.OSM()}));
    for(let k = 0; k < camadasRastrear.length; k++){
        mapRastrear.addLayer(camadasRastrear[k]);
    }
    if(tracarCaminho){
        let i;
        let prevP = null;

        for(i = 0; i < seletorCicloRastrear.value; i++){
            if(prevP == null){
                viewPointCiclo(i);
            }else{
                makeLine(prevP, i);
                if(mantemPontosRastrear || i == seletorCicloRastrear.value -1){
                    viewPointCiclo(i);
                }
            }
            prevP = i; 
        }
    }else{
        for(var i = 0; i < ciclosVisiveis.length; i++){
            if (ciclosVisiveis[i] == true)
                viewPointCiclo(i);
        }
    }
}

function loadViewMapRastreamento(){
    mapRastrear = new ol.Map({
        target: 'divMapaRastrear',
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM()
            })
        ],
        view: viewRastrear
    });
}

function viewPointCiclo(nCiclo){
    let ponto = coordenadasRastreamento[nCiclo];
    let centerShape = new Object();
    let vectorSource = new ol.source.Vector();
    let iconFeature = new ol.Feature(new ol.geom.Point([ponto['x'], ponto['y']]));
    vectorSource.addFeature(iconFeature);
    centerShape.lat = ponto['x'];
    centerShape.lng = ponto['y'];
    let st = createSt();
    let vectorLayerPontos = new ol.layer.Vector({ source: vectorSource, style: st });
    mapRastrear.addLayer(vectorLayerPontos);
}

function centerPoint(nCiclo){
    viewRastrear.centerOn([coordenadasRastreamento[nCiclo].x , coordenadasRastreamento[nCiclo].y], mapRastrear.getSize(), [500, 200]);
}


function updateCiclo(){
    let cicloAt = seletorCicloRastrear.value - minCicloRastreamento;
    cicloAtualRastreamento = parseInt(seletorCicloRastrear.value);
    
    if(mantemPontosRastrear){
        ciclosVisiveis[cicloAt] = true;
        atualizaMapaRastrear();
    }
    else{
        for(let  i = 0; i < ciclosVisiveis.length; i++)
            ciclosVisiveis[i] = false;
        ciclosVisiveis[cicloAt] = true;
        atualizaMapaRastrear();
    }
    if(zoomPosAtual){
        centerPoint(cicloAt);
    }
    
    loadViewTabelaRastreamento(cicloAt);
}

btnRastrear.addEventListener("click", function(){
    buscaRastreamento();
})

seletorCicloRastrear.addEventListener("change", function(){
    updateCiclo();
})

function animacao(){
    if(stopAnim != true && cicloAtualRastreamento < (minCicloRastreamento+nCiclosRastreamento - 1)){
        updateCiclo();
        seletorCicloRastrear.value++;
    }
    setTimeout(animacao, 900);
}
//1797931303


function jumpCiclo(){ // avança para um ciclo especificado no inputJumpCiclo
    let ciclo = inputJumpCiclo.value;
    cicloAtualRastreamento = ciclo;
    seletorCicloRastrear.value = ciclo;        
    updateCiclo();
}

function playAnimacao(){
    stopAnim = false;
    animacao();
}

function pauseAnimacao(){
    stopAnim = true;
}

function makeLine(ini, fim){
    let x1 = parseFloat(coordenadasRastreamento[ini].x)
    let y1 = parseFloat(coordenadasRastreamento[ini].y)
    let x2 = parseFloat(coordenadasRastreamento[fim].x)
    let y2 = parseFloat(coordenadasRastreamento[fim].y)

    var points = [ [x1, y1], [x2, y2] ];

    var featureLine = new ol.Feature({
        geometry: new ol.geom.LineString(points)
    });

    var vectorLine = new ol.source.Vector({});
    vectorLine.addFeature(featureLine);

    var vectorLineLayer = new ol.layer.Vector({
        source: vectorLine,
        style: new ol.style.Style({
            fill: new ol.style.Fill({ color: '#00FF00', weight: 4 }),
            stroke: new ol.style.Stroke({ color: '#00FF00', width: 2 })
        })
    });

    mapRastrear.addLayer(vectorLineLayer);
}
//============================================================ carregamento de shapes

function AdcLayerRastrear() {
    var camada = mapasRastrear.value;
    toggleLoaderMap(true);
    $.ajax({
        type: "POST",
        url: "../app/models/VerificaTipoLayer.php",
        dataType: "json",
        data: { layer: camada },
        success: function(data) {
            if (data[0].erro) {
                alerta("Erro ao carregar", "Individual. Não foi possivel encontrar o shape desejado : "+data.erro);
            } else {
                if (data == "POLIGONO")
                    CarregaMapa(camada, mapRastrear, camadasRastrear, false);
                else
                    CarregaPontos(camada, mapRastrear, camadasRastrear, false);
                    toggleLoaderMap(false);
            }
        },
        error: function(jqXHR, textStatus, errorThrown, data) {
            alerta("Erro ao carregar", "Individual. Erro na busca ao banco de dados!");
            console.log(errorThrown);
            console.log(data);
        }
    });
}


//============================================================
btnMantemPontosRastrear.onchange = atualizaValoresVisualizacao;
btnTracarCaminho.onchange        = atualizaValoresVisualizacao;
btnZoomPosAtual.onchange         = atualizaValoresVisualizacao;
btnOsmRastrear.onchange          = atualizaValoresVisualizacao;
btnIrJumpCiclo.onclick           = jumpCiclo;
btnPlayRastreamento.onclick      = playAnimacao;
btnPauseRastreamento.onclick     = pauseAnimacao;

atualizaValoresVisualizacao();