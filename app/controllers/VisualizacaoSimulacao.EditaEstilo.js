function editaEstiloSimulacao(){
    var sizeIcone = document.getElementById("escalaIcone");
    
    escala = sizeIcone.value / 1000;

    atualizaCiclo();
}

var canvas = $('canvas').get(0);

function setDPI(canvas, dpi) {
  var scaleFactor = dpi / 96;
  canvas.width = Math.ceil(canvas.width * scaleFactor);
  canvas.height = Math.ceil(canvas.height * scaleFactor);
var ctx=canvas.getContext("2d");
  ctx.scale(scaleFactor, scaleFactor);
}



function tiraPrint(){
  var zip = new JSZip();

    var inicio = document.getElementById("inicioImagens").value;
    var fim    = document.getElementById("fimImagens");

    if(inicio == null || fim  == null || fim < inicio){
        alerta("Erro", "Intervalo Invalido!");
        return false;
    }
        
    cicloAtualInput.value = inicio;
    atualizaCiclo();

    setTimeout(function(){
        printt(zip,fim);
    }, 1000);

    return false;
};

function printt(zip,fim){
      map.once('postcompose', function(event) {
        var canvas = event.context.canvas;    
            console.log("aqui");
              console.log("ASQSqS");
              if (navigator.msSaveBlob) {
              } else {
                canvas.toBlob(function(blob) {
                  map.renderSync();
                  zip.file("map"+cicloAtualInput.value+".png", blob, {base64: true});
                  console.log(zip);
                  cicloAtualInput.value++;
                  atualizaCiclo();
                  map.renderSync();
                  if(cicloAtualInput.value >= fim.value){
                    setDPI(canvas,300);
                    zip.generateAsync({type:"blob"}).then(function(content) {
                      saveAs(content, "archive.zip");
                    })
                    cicloAtualInput.value = 0;
                    return;
                  }
                  console.log("ini : "+ cicloAtualInput.value + " fim : " + fim.value)
                  setTimeout(function(){ 
                    printt(zip,fim);
                  }, 1000);
                });
              }  
      });
      map.renderSync();
}

//evento de ajuste de zoom

function AlteraZoom(){
    hideContextMenu();
    var zoom = map.getView().getZoom();
    if(zoom <=11)
      escala = 0.3 * (zoom / 50);
    else if(zoom <=15)
      escala = 0.3 * (zoom / 30);
    else
      escala = 0.3 * (zoom / 20);
    atualizaCiclo();
};





