var StackSimulacoes = [];
var simul = false;
var cicloAtualInput = document.getElementById("cicloAtual");
var cicloTotalInput = document.getElementById("cicloTotal");
var individuosSelecionados = [];
var setIndividuosSelecionados = new Set();
var individuos = [];
var escala = 0.3;
var IZoom = false;

///////////////////////controle de ciclos\\\\\\\\\\\\\\\\\\\\\\\

function NextCiclo(){ // avança um ciclo e atualiza a visualização
    console.log(simul)
    if((cicloAtualInput.value +1 <= 365) && simul){
        cicloAtualInput.value++;
        atualizaCiclo();
    }
}
function PrevCiclo(){ // retrocede um ciclo e atualiza a visualização
    if((cicloAtualInput.value -1 >= 1) && simul){
        cicloAtualInput.value--;
        atualizaCiclo();
    }
}
function atualizaCiclo(){ // atualiza todas as simulações na visualização
    var ciclo = cicloAtualInput.value -1;
    if(simul){
        for(var  i = 0 ; i < StackSimulacoes.length ; i++)
            carregaPontosCiclo(StackSimulacoes[i],ciclo);
    }
}
///////////////////////filtros\\\\\\\\\\\\\\\\\\\\\\

function selecionaIndividuos(){

    var ciclo = cicloAtualInput.value;
    for(var  i = 0 ; i < StackSimulacoes.length ; i++)
        carregaPontosCiclo(StackSimulacoes[i],ciclo);
}

function findNomeAgente(cod){
    for(var  i = 0 ; i < individuos.length ; i++){
        if(individuos[i].cod == cod)
            return individuos[i].nome;
    }
};

///////////////////////carregamento da simulação\\\\\\\\\\\\\\\\\\\\\\\

function createSt(src) { //cria o estilo com o icone do agente 
    return new ol.style.Style({
        image: new ol.style.Icon(({
            src: src,
            scale: escala
        }))
    });
}

function selectSimulacao(){
    var arquivo = document.getElementById("simulacoes").value;

    var found = StackSimulacoes.find(function(element) {
        return element == arquivo;
    });

    if(!found){
        cicloAtualInput.removeAttribute('readonly');
        cicloAtualInput.value = 1;
        simul = true;

        atualizaTotalCiclos(arquivo);

        carregaPontosCiclo(arquivo,0);
    }
    else{
        alert("O arquivo já esta presente na visualização");
    }   
}


function carregaPontosCiclo(arquivo,ciclo){
    var individuosLoad;
    var center;

    var found = false;

    for(var  i = 0 ; i < StackSimulacoes.length ; i++){
        if(arquivo == StackSimulacoes[i])
            found = true;
    }
    


    
    if(found == false)
        individuosLoad = [];
    else
        individuosLoad = retorna_individuos();




    $.ajax({
        type: "POST",
        url: "../app/models/CarregaPontosSimulacao.php",
        dataType: "json",
        data: { 'arquivo': arquivo , 'ciclo': ciclo , 'individuos':individuosLoad},
        success: function(data) {  
            var existe = false;
            var indice;
            for (var i = 0; i < visivel.length; i++) {
                if (visivel[i][2] == arquivo) {
                    existe = true;
                    indice = visivel[i][0];
                }
            }

            var vectorSource = new ol.source.Vector();
            for (var i = 0; i < data.length; i++) {
                ponto = data[i].coord.split(' ');
                var iconFeature = new ol.Feature(new ol.geom.Point([ponto[0], ponto[1]]));
                center = ponto;

                iconFeature.set('style', createSt('../../imagens/icones/'+  retornaIcone(data[i].icone)+'.png'));
                vectorSource.addFeature(iconFeature);

                if(!setIndividuosSelecionados.has(data[i].icone)){
                    setIndividuosSelecionados.add(data[i].icone);

                    var ind = individuosSelecionados.length;
                    individuosSelecionados[ind] = new Object();
                    individuosSelecionados[ind].cod  = retornaIcone(data[i].icone);
                    individuosSelecionados[ind].nome = findNomeAgente(data[i].icone);
                }
            }    
            var vectorLayerPontos = new ol.layer.Vector({ source: vectorSource, style :function(feature){return feature.get('style'); }});
            vectorLayerPontos.set('name', arquivo);




            if(existe)//se a simulação já está presente na view apenas a atualiza
                atualizaSimulacao(vectorLayerPontos,indice);
            else //adiciona a simulação a view
                insereSimulacao(vectorLayerPontos,arquivo);
            loadSelectIndividuosSelecionados();
            if(!found){
                StackSimulacoes.push(arquivo);
                view.centerOn([ponto[0] , ponto[1]], map.getSize(), [500, 200]);
            }
        },
        error: function(errorThrown, data) {
            alert('Erro ao Carregar!');
            console.log(errorThrown);
            console.log(data);
        }
    });
}

function atualizaTotalCiclos(arquivo){
    $.ajax({
        type: "POST",
        url: "../app/models/RetornaNumeroCiclos.php",
        dataType: "json",
        data: { 'arquivo': arquivo},
        success: function(data) {
            cicloTotalInput.value =  data.count - 2 ;
        },
        error: function(errorThrown, data) {
            alert('Erro ao Carregar!');
            console.log(errorThrown);
            console.log(data);
        }
    });
}

function insereSimulacao(vectorLayerPontos,arquivo){
    map.addLayer(vectorLayerPontos);
    visivel.push(0);
    camadas.push(vectorLayerPontos);
    visivel[visivel.length - 1] = new Array();
    visivel[visivel.length - 1].push(camadas.length - 1);
    visivel[visivel.length - 1].push(true);
    visivel[visivel.length - 1].push(arquivo);
    visivel[visivel.length - 1].push(true);
    visivel[visivel.length - 1].push("simulacao");

    document.getElementById("filtro").style.display = "";
    AdicionaIconeMapa(arquivo,true);
}

function atualizaSimulacao(vectorLayerPontos,indice){
    camadas[indice] = vectorLayerPontos;
    atualizaMapa();
}

function RemoveSimulacao(nomesimulacao){
    for(var i = 0 ; i < StackSimulacoes.length ; i++){
        console.log(StackSimulacoes[i]);
        if(StackSimulacoes[i] == nomesimulacao)
            StackSimulacoes.splice(i,1);
    }

    if(StackSimulacoes.length == 0){
        simul = false;
        cicloAtualInput.value = 0;
        cicloTotalInput.value = 0;
        cicloAtualInput.setAttribute('readonly',true);
        document.getElementById("filtro").style.display = "none";
    }
}