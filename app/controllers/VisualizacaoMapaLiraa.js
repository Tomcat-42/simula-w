//=============================================================== componentes
var selectTabelasLiraa      = document.getElementById("selectTabelasLiraa");
var selectMapasLiraa        = document.getElementById("selectMapasLiraa");
var selectColunaTabelaLiraa = document.getElementById("selectColunaTabelaLiraa");
var legendaLiraAtual        = document.getElementById("legendaLiraAtual");

var liraaCorSatisfatorio    = document.getElementById("liraaCorSatisfatorio");
var liraaCorAlerta          = document.getElementById("liraaCorAlerta");
var liraaCorRisco           = document.getElementById("liraaCorRisco");

//=============================================================== Dados
var liraaAtualTabela  = null;
var liraaAtualMapa    = null;

var liraaAtualColunas = null;

var liraaDadosQuadras = null;
var liraaDadosColuna  = null;

var liraaAtualQuadrasMap = [];
//=============================================================== carregamento de dados

function carregaTabelas(){
    $.ajax({
        type : "POST",
        url : "../app/models/visualizacao/mapas/liraa/loadTabelasFonte.php",
        dataType : "json",
        success:function(data){
            Simulacoes = data;
            for(let  i = 0 ; i < Simulacoes.length ; i++){
                let option = document.createElement("option");
                option.innerHTML = Simulacoes[i]['tablename'];
                selectTabelasLiraa.appendChild(option);
            }
        },
        error: function(data){

        }
    })
    selectTabelasLiraa
}

function pegaInformacoesTabela(){
    if(selectTabelasLiraa.value != ""){
        $("#carregamentoMapaLira").modal("show");
        liraaAtualTabela = selectTabelasLiraa.value;
        $.ajax({
            url : "../app/models/visualizacao/mapas/liraa/loadInformacoesQuadras.php",
            type : "POST",
            dataType: "json",
            data : {tabela : liraaAtualTabela},
            success : function(data){
                liraaDadosQuadras = data['quadras'];
                liraaAtualColunas = data['colunas'];
                populaSelectColunas();
            },
            error : function(){
                alerta("LIRAa", "Visualização. Erro no carregamento dos dados da tabela " + liraaAtualTabela + "!");
            }
        });
        selectTabelasLiraa.value = "";
    }
    else{
        alerta("LIRAa", "Visualização. Selecione uma tabela com os dados LIRAa desejados.");
    }
}

function pegaInformacoesMapa(){
    if(selectMapasLiraa.value != ""){
        $("#selecaolira").modal("show");
        liraaAtualMapa = selectMapasLiraa.value;
        atualizaLegendaLiraaAtual();
        selectTabelasLiraa.value = "";
    }
    else{
        alerta("LIRAa", "Visualização. Selecione um mapa para visualizar os dados!");
    }
}

function atualizaLegendaLiraaAtual(){
    var colunaAt = "";
    if(selectColunaTabelaLiraa.value != "--");
        colunaAt = selectColunaTabelaLiraa.value;
    legendaLiraAtual.innerHTML = "Mapa : " + liraaAtualMapa + "<br/>LIRAa : " + liraaAtualTabela + "<br/>Coluna: " + colunaAt;
}

function populaSelectColunas(){
    $(selectColunaTabelaLiraa).empty();
    let option = document.createElement("option");
    option.innerHTML = "--";
    selectColunaTabelaLiraa.appendChild(option);
    for(let i = 0; i < liraaAtualColunas.length; i++){
        let option = document.createElement("option");
        option.innerHTML = liraaAtualColunas[i];
        selectColunaTabelaLiraa.appendChild(option);
    }
}
//=============================================================== carregamento do mapa
function pegaInformacoesColunaAtual(){
    if(selectColunaTabelaLiraa.value != ""){
        $.ajax({
            url : "../app/models/visualizacao/mapas/liraa/loadInformacoesLiraa.php",
            type : "POST",
            dataType: "json",
            data : {tabela : liraaAtualTabela, coluna : selectColunaTabelaLiraa.value},
            success : function(data){
                toggleLoaderMap(true);
                for(let i = 0; i < liraaDadosQuadras.length; i++)
                    liraaAtualQuadrasMap[liraaDadosQuadras[i]] = data[i];
                let nome = selectColunaTabelaLiraa.value + " (" +   liraaAtualTabela + ")";
                CarregaMapa(liraaAtualMapa, map, camadas, true, true, nome);
                toggleLoaderMap(false);
            },
            error : function(){
                alerta("LIRAa", "Visualização. Erro no carregamento dos dados da tabela " + liraaAtualTabela + "!");
            }
        });
    }
    else{
        alerta("LIRAa", "Visualização. Selecione uma coluna com os dados LIRAa desejados.");
    }
}

//=============================================================== Estilização
var satisfatorioSt = new ol.style.Style({
    fill: new ol.style.Fill({
        color: rgba( 7, 249, 41, 1)
    }),
    stroke: new ol.style.Stroke({
        color: '#000000',
        width: 0.5
    })
});

var alertaSt = new ol.style.Style({
    fill: new ol.style.Fill({
        color: rgba( 199, 249, 7, 1)
    }),
    stroke: new ol.style.Stroke({
        color: '#000000',
        width: 0.5
    })
});

var riscoSt = new ol.style.Style({
    fill: new ol.style.Fill({
        color: rgba(249, 7, 7, 1)
    }),
    stroke: new ol.style.Stroke({
        color: '#000000',
        width: 0.5
    })
});



//=============================================================== pré-carregamento de dados

carregaTabelas();
LoadMapsFromDb(selectMapasLiraa);
selectColunaTabelaLiraa.onchange = atualizaLegendaLiraaAtual;
liraaCorSatisfatorio.value =  "#07f929";
liraaCorAlerta.value       =  "#c7f907";
liraaCorRisco.value        =  "#f90707";
