//============================================================ componentes
var divMapaRastrear            = document.getElementById("divMapaRastrear");
var btnRastrear                = document.getElementById("btnRastrear");
var divAtributosAgenteRastrear = document.getElementById("tabelaAtributosAgenteRastrear");
var nCicloRastreamento         = document.getElementById("nCicloRastreamento");
var tabRastrear                = document.getElementById("rastrear-tab");

var seletorCicloRastrear      = document.getElementById("seletorCicloRastrear");
var cicloAtualLegendaRastrear = document.getElementById("cicloAtualLegendaRastrear");

var inputIdRastrear       = document.getElementById("inputIdRastrear");
var inputCicloRastrear    = document.getElementById("inputCicloRastrear");

var inputHumanoRastrear   = document.getElementById("inputHumanoRastrear");
var inputMosquitoRastrear = document.getElementById("inputMosquitoRastrear");
//============================================================ dados
var dadosTabelaAtualRastreamento = [];
var tabelasDadosRastreamento = [];
var minCicloRastreamento;
var nCiclosRastreamento;

var cicloAtualRastreamento;

var camadasRastrear = [];
var coordenadasRastreamento;
//============================================================ Tabela Atributos
var tabelaAtributosRastreamento = new Tabulator(divAtributosAgenteRastrear,{
    data: dadosTabelaAtualRastreamento,
    height:"230px",
    width:"1px",
    layout:"fitColumns", 
    responsiveLayout:"hide",
    tooltips:true,
    history:true,
    resizableRows:false,
    columns:[
        {title:"Atributos", field:"campo", headerSort:false, align:"left", width:140},
        {title:"Valores", field:"valor", headerSort:false, align:"left", width: 140}
    ]
});;
//============================================================ controllers ciclos

function atualizaDadosBuscaRastreamento(nciclos, minCiclo){ 
    // atualiza os indicadores de ciclos na barra inferiror e acima da tabela 
    minCicloRastreamento = minCiclo;
    nCiclosRastreamento = nciclos;
    let maxCiclo = (minCiclo + nciclos) - 1;

    cicloAtualLegendaRastrear.innerHTML = "Ciclos " + minCiclo + " - " + maxCiclo;
    seletorCicloRastrear.min = minCiclo;
    seletorCicloRastrear.max = maxCiclo;
}

function buscaRastreamento(){ // obtem os parametros do rastreamento e requisita a busca
    let id = inputIdRastrear.value;
    let ciclo = inputCicloRastrear.value;
    let agente = null;

    if(inputHumanoRastrear.checked == true)
        agente = "Humano";
    else if(inputMosquitoRastrear.checked == true)
        agente = "Mosquito";
    $.ajax({
        type : "POST",
        url : "../app/models/individual/IndividualRastreiaIndividuo.php",
        dataType: "json",
        data : {'ciclo' : ciclo, 'agente' : agente, 'id' : id},
        async : false,
        success: function(data){
            if(data == "erro")
                alerta("Erro", "Individual. Os dados inseridos não correspondem a nenhum individuo");
            else{
                tabelasDadosRastreamento = loadTabelasDadosRastreamento(data, agente);
                coordenadasRastreamento  = processaPontos(loadCoordenadasRastreamento(data));
                loadViewTabelaRastreamento(0);
                cicloAtualRastreamento = 0;
                if( mapRastrear == null)
                    loadViewMapRastreamento();
                updateCiclo();
                centerPoint(0);
            }
        },
        error: function(){
            alerta('Erro','Individual. Erro ao Carregar!');
            console.log(errorThrown);
        }
    });
}

function processaPontos(pontos_at){ // formata os pontos alterando o src 
    let pontosProcessados;
    $.ajax({
        type : "POST",
        url : "../app/models/individual/ProcessaPontos.php",
        dataType: "json",
        data : {'pontos' : pontos_at},
        async : false,
        success: function(data){
            pontosProcessados = data;
            for(let  i = 0; i < pontosProcessados.length; i++)
                ciclosVisiveis.push(false);
        },
        error: function(){
            alerta('Erro','Individual. Erro ao Carregar!');
            console.log(errorThrown);
        }
    });
    return pontosProcessados;
}

function loadTabelasDadosRastreamento(dados, agente_at){ // 1234289174
    let legendas;
    let atributos;
    let nciclos;
    let minCiclo;

    let tabela = [];

    if(agente_at == "Humano")
        legendas = mapLegendasAtributosHumanos;
    else
        legendas = mapLegendasAtributosMosquitos;

    console.log(legendas);


    nciclos   = Object.keys(dados['ciclo']).length;
    minCiclo  = dados['ciclo'][0];
    atributos = Object.keys(dados);
    console.log("lalalala");
    console.log(atributos);
    for(let i = 0; i < atributos.length; i++){
        let campoAt;
        if(legendas[atributos[i]] != null)
            campoAt = legendas[atributos[i]];
        else 
            campoAt = atributos[i];
        for(let j = 0; j < Object.keys(dados[atributos[i]]).length; j++){
            if(i == 0)
                tabela[j] = [];    
            tabela[j].push({
                'campo' : campoAt,
                'valor' : dados[atributos[i]][j]
            }); 
        }   
    }
    atualizaDadosBuscaRastreamento(nciclos, minCiclo);
    return tabela;
}

function loadCoordenadasRastreamento(dados){ // 1207067312
    let tabela = [];

    for(let i = 0; i < Object.keys(dados['x']).length; i++){
        tabela[i] = [];    
        tabela[i].push({
            'x' : dados['x'][i],
            'y' : dados['y'][i]
        });    
    }
    return tabela;
}

//============================================================ eventos

function loadViewTabelaRastreamento(){ // atualiza a tabela com valor do ciclo atual
    let ciclo = parseInt(seletorCicloRastrear.value);
    let cicloAt = ciclo - minCicloRastreamento;
    tabelaAtributosRastreamento.replaceData(tabelasDadosRastreamento[cicloAt]);
    nCicloRastreamento.innerHTML = "Ciclo " + ciclo;
}

function updateCiclo(){
    let cicloAt = seletorCicloRastrear.value - minCicloRastreamento;
    cicloAtualRastreamento = parseInt(seletorCicloRastrear.value);
    
    if(mantemPontosRastrear){
        for(let  i = 0; i <= cicloAt; i++)
            ciclosVisiveis[i] = true;
        for(let  i = cicloAt+1; i < ciclosVisiveis.length; i++)
            ciclosVisiveis[i] = false;
        atualizaMapaRastrear();
    }
    else{
        for(let  i = 0; i < ciclosVisiveis.length; i++)
            ciclosVisiveis[i] = false;
        ciclosVisiveis[cicloAt] = true;
        atualizaMapaRastrear();
    }
    if(zoomPosAtual){
        centerPoint(cicloAt);
    }
    
    loadViewTabelaRastreamento(cicloAt);
}

btnRastrear.addEventListener("click", function(){
    buscaRastreamento();
})

seletorCicloRastrear.addEventListener("change", function(){
    updateCiclo();
})