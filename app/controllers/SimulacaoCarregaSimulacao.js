/*
    Matheus N Ismael 15/03/19
*/

var buttonInicia = document.getElementById("carregarSimulacaoEntradas");
var buttonNova   = document.getElementById("buttonCarregaNova");

var boxCont           = document.getElementById("sidebar-boxSimulacao");
var sidebarTitulo     = document.getElementById("sidebar-titulo");
var sidebarAmbiente   = document.getElementById("sidebar-ambiente");
var sidebarDoenca     = document.getElementById("sidebar-doenca");
var sidebarDuracaoAno = document.getElementById("sidebar-duracao");
var sidebarStatus     = document.getElementById("sidebar-status");

//================================================================

function carregaParametros(simulacaoAtTitulo){
    loadTabela("humanos", simulacaoAtTitulo);
    loadTabela("mosquitos", simulacaoAtTitulo);
    loadTabela("simulacao", simulacaoAtTitulo);
    loadTabelaAmbientais(simulacaoAtTitulo); 

    plotaTabelas();
}

function separador(){
    var hr = document.createElement("hr");
    return hr;
}

function carregaSidebar(simulacaoAT){
    boxCont.style.display       = "block";
    sidebarTitulo.innerHTML     = simulacaoAT['nome'].toUpperCase();
    sidebarTitulo.appendChild(separador());
    sidebarStatus.innerHTML     = simulacaoAT['status'].toUpperCase();
    sidebarStatus.appendChild(separador());
    sidebarAmbiente.innerHTML   = simulacaoAT['ambiente'].toUpperCase();
    sidebarAmbiente.appendChild(separador());
    sidebarDoenca.innerHTML     = simulacaoAT['doenca'].toUpperCase();
    sidebarDoenca.appendChild(separador());
    sidebarDuracaoAno.innerHTML = simulacaoAT['duracaoano'];
    sidebarDuracaoAno.appendChild(separador());
}

function hideSidebar(){
    boxCont.style.display = "none";
    sidebarTitulo.innerHTML     = "";
    sidebarStatus.innerHTML     = "";
    sidebarAmbiente.innerHTML   = "";
    sidebarDoenca.innerHTML     = "";
    sidebarDuracaoAno.innerHTML = "";
}

function carregaSimulacao(){
    var simulacaoAT = simulacaoAtual();
    if(simulacaoAT != null){
        exibeMenu(false);
        exibeExecuta(true);
        var simulacaoAtTitulo = inputSimulacao.value;
        carregaParametros(simulacaoAtTitulo);
        carregaSidebar(simulacaoAT);
    }
    else
        alerta("Carregar","Simulação. Selecione uma simulacao para ser carregada.");
}

function carregaSimulacaoNova(){
    carregaInfosNova();
    exibeMenu(true);
    exibeExecuta(false);
    var base = simulaBase.value;
    carregaParametros(base);
    carregaSidebar(simulacaoNova);
}

buttonInicia.addEventListener('click',carregaSimulacao);
buttonNova.addEventListener('click',carregaSimulacaoNova);