/*
    Matheus Nunes 08/2018
*/

var statusBox;
$(function() {
    $('.submit_grafico_tabela').on('click', function() {
        var file_data = $('.image').prop('files')[0];
        var files = $('.image').prop('files');

        if (file_data != undefined) {
            var n_simulacao = document.getElementById("simulacao_tabela").value;

            for(var  l = 0 ; l < files.length ; l++){

                //--------------------------------

                var divUpl = document.createElement("div");
                $(divUpl).addClass("uploadItem");
                divUpl.innerHTML = files[l].name;
                    var divStatus = document.createElement("div");
                        $(divStatus).addClass("spanCarregando");

                        divStatus.innerHTML = "carregando";
                    divUpl.appendChild(divStatus);
                statusBox.appendChild(divUpl);

                //--------------------------------

                var form_data = new FormData();

                form_data.append('file',files[l]);
                form_data.append('simulacao', n_simulacao);


                $.ajax({
                    type: 'POST',
                    url: '../app/models/visualizacao/graficos/uploadGraph.php',
                    contentType: false,
                    processData: false,
                    dataType: "json",
                    data: form_data,
                    enctype: "multipart/form-data",
                    success: function(data) {
                        if (data == "success") {
                            $(divStatus).empty();
                            $(divStatus).removeClass();
                            $(divStatus).addClass("spanCarregado");
                            divStatus.innerHTML = "carregado";
                        } else if (data == "duplicado") {
                            $(divStatus).empty();
                            $(divStatus).removeClass();
                            $(divStatus).addClass("spanDuplicado");
                            divStatus.innerHTML = "duplicado";
                        } else {
                            $(divStatus).empty();
                            $(divStatus).removeClass();
                            $(divStatus).addClass("spanErroCarregamento");
                            divStatus.innerHTML = "erro no carregamento";
                        }
                    },
                    error: function(errorThrown, data) {
                        $(divStatus).empty();
                        $(divStatus).removeClass();
                        $(divStatus).addClass("spanErroServidor");
                        divStatus.innerHTML = "erro no envio";
                        console.log(errorThrown);
                        console.log(data);
                    }
                });
            }
            return false;
        }
        return false;
    });
});
