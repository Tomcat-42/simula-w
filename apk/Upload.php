<?php
    include_once("../app/models/conecta-simula.php");
    include_once("../app/models/conecta.php");
    
    include_once("UploadGraficos.php");
    include_once("UploadSimulacao.php");
    include_once("../app/models/simulacao/SimulacaoFuncoes.php");

    $pastaGraficos    = "AEDES_Acoplado/Saidas/MonteCarlo_0/";
    $pastaSimulacao    = "AEDES_Acoplado/Saidas/MonteCarlo_0/Simulacao_0/";

    $simulacao = retornaEmExecucao($conn);

    echo("\n\n\n\n[".$simulacao."]\n\n\n\n\n");


    updateStatus($simulacao, "Carregando Resultados", $conn);

    UploadGraficos($pastaGraficos, $simulacao, $conn_simula);
    UploadSimulacao($pastaSimulacao, $simulacao, $conn_simula);

    updateStatus($simulacao, "Processado", $conn);
?>
