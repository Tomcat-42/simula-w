<?php
    function UploadGraficos($pasta, $simulacao, $conn_simula){
        $prefixos = ["_Humanos_Novo_","_Mosquitos_Dengue_Novo_","_Mosquitos_Wolbachia_Novo_","_Humanos_","_Mosquitos_Dengue_","_Mosquitos_Wolbachia_"];
        $tabelas  = ["_Humanos_Novo","Mosquitos_Dengue_Novo","Mosquitos_Wolbachia_Novo","Humanos","Mosquitos_Dengue","Mosquitos_Wolbachia"];
        $qQuadras = [361, 151, 21, 361, 151, 21];
        for($y = 0 ; $y < sizeof($prefixos) ; $y++){

            //cria tabelas
            $tabelaAgente    = $simulacao.$tabelas[$y];
            $sqlTabelaAgente = "CREATE TABLE $tabelaAgente(quadra integer, " ;
            for($i = 0 ; $i < $qQuadras[$y] ; $i++){
                $sqlTabelaAgente .= " a$i integer";
                if($i < $qQuadras[$y]-1)
                    $sqlTabelaAgente .= ",";
            }
            $sqlTabelaAgente .= ")";
            $qry = pg_query($conn_simula,$sqlTabelaAgente);

            //lê e insere dados
            $quadra = 0;
            while($handle = fopen($pasta."Quantidades".$prefixos[$y]."Quadra-".$quadra.".csv", "r")){
                while($line = fgetcsv($handle,3500,";")) {
                    $sql  = "INSERT INTO $tabelaAgente VALUES (";
                    $sql .= $quadra.",";
                    for($i = 0 ; $i < $qQuadras[$y] ; $i++){
                        $sql .= "'".$line[$i]."'";
                        if($i < $qQuadras[$y]-1)
                            $sql .= ",";
                    }
                    $sql .= ");";
                    $qry = pg_query($conn_simula,$sql);
                    echo $sql . " \n";
                }
                echo $quadra . "\n";
                $quadra++;
            }
        }
    }
?>
