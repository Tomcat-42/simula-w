<?php
    function UploadSimulacao($pasta, $simulacao, $conn_simula){
        $types = array('csv');
        if ( $hnd = opendir($pasta) ) {
            while ( $entry = readdir( $hnd ) ) {
                $ext = strtolower( pathinfo( $entry, PATHINFO_EXTENSION) );
                if( in_array( $ext, $types ) ){
                    //echo $entry;

                    $nome = $simulacao."_".$entry;
                    $nome = substr($nome , 0 , stripos($nome, "."));

                    $handle = fopen($pasta.$entry, "r");
                    $first = ftell($handle);
                    $line = fgetcsv($handle,3500,";");
                    $nciclos = count($line)-2;
                    fseek($handle, $first, SEEK_SET);

                    $sql = "CREATE TABLE $nome(x int NOT NULL, y int NOT NULL,";
                    for($i = 0 ; $i < $nciclos ; $i++){
                        $sql .= "c$i int";
                        if($i < $nciclos -1)
                            $sql .= ",";
                    }
                    $sql .= ");";
                    $tst = 0;
                    $qry = pg_query($conn_simula,$sql);
                    if($qry == false)
                        $recall= "duplicado";
                    else
                    {
                        while ($line = fgetcsv($handle,3500,";")) {
                            $tst++;
                            $sql = "INSERT INTO $nome VALUES (";
                            for($i = 0 ; $i < $nciclos ; $i++){
                                $sql .= "'".$line[$i]."'";
                                if($i < $nciclos-1)
                                    $sql .= ",";
                            }
                            $sql .= ");";
                            $qry = pg_query($conn_simula,$sql);
                        }
                        $recall = "success";
                    }
                    echo json_encode($recall);
                    fclose($handle);
                } 
            }
            closedir($hnd);
        }
    }
?>
