#!/usr/bin/env python3

from ambiente import Ambiente
from movimentacao import Movimentacao
from controle import Controle
from climatico import Climatico
from distribuicao import Distribuicao

import pandas as pd
import time
import sys
import banco

def main():
	simul =  sys.argv[1]
	path = sys.argv[2]
	simul_ambiente = path+"/../../../apk/AEDES_Acoplado/Entradas/MonteCarlo_0/Ambiente"
	ref_time = time.time()
	print('Lendo parâmetros...')
	params = banco.ler_parametros(simul)
	amb = Ambiente(params,simul_ambiente)
	amb.gerar_arquivo_saida()
	mov = Movimentacao(params, amb, simul_ambiente)
	mov.gerar_arquivo_saida()
	con = Controle(params, amb, simul_ambiente)
	con.gerar_arquivo_saida()
	con.gerar_arquivo_csv_focos()
	cli = Climatico(params, simul_ambiente)
	cli.gerar_arquivo_saida()
	dist = Distribuicao(params, amb, simul_ambiente)
	dist.gerar_arquivo_saida_humanos()
	dist.gerar_arquivo_saida_mosquitos()
	print('Tempo total:', time.time() - ref_time)

if __name__ == '__main__': main()
