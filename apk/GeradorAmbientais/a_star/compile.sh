#!/bin/sh

echo "Compilando a_star.cpp"
g++ -c -fPIC a_star.cpp -o a_star.o
g++ -shared -Wl,-soname,a_star.so -o a_star.so a_star.o
rm a_star.o
