#!/usr/bin/env python3

import pandas as pd
import numpy as np
import sys
import banco

def get_regiao(quadra):
	if quadra.startswith('0'):
		return 'RUA'
	if quadra.startswith('G'):
		return 'RURAL'
	if quadra.startswith('Q'):
		return 'QUADRA'
	return None

def print_arr(arr, sep=';'):
	"""Imprime uma string formatada com os elementos de um vetor."""
	strings = [str(i) for i in arr]
	print(sep.join(strings))

def print_arr_np(arr, sep=';'):
	"""Imprime uma string formatada com os elementos de um vetor."""
	if type(arr) is not np.ndarray:
		arr = np.asarray(arr)
	arr.tofile(sys.stdout, sep=sep)
	print()  # Nova linha

class Ambiente:

	__slots__ = ['params', 'pontos', 'lotes', 'index_quadras', 'index_lotes',
				 'viz', 'esquinas', 'centroides', 'fronteiras', 'simul']

	def __init__(self, params, simul):
		print('Processando ambiente...')
		self.params = params
		self.simul = simul
		self.ler_dados()
		self.proc_index_quadras()
		self.simul = simul;
		self.proc_posicoes()
		self.proc_fronteiras()
		self.proc_vizinhancas()
		self.proc_esquinas()
		self.proc_centroides_esquinas()

	def ler_dados(self):
		"""Acessa os dados ambientais do banco de dados."""
		ambiente = self.params['Ambiente']
		self.pontos = banco.ler_pontos(ambiente)
		self.lotes = banco.ler_lotes(ambiente)
		self.viz = banco.ler_vizinhancas(ambiente)
		self.esquinas = banco.ler_esquinas(ambiente)
		self.centroides = banco.ler_centroides_esquinas(ambiente)

	def proc_index_quadras(self):
		"""Gera um DICT com índices de quadras e lotes."""
		self.index_quadras = {}
		self.index_lotes = {}
		id_quadra = 0
		for quadra in self.lotes.quadra.unique():
			self.index_quadras[quadra] = id_quadra
			self.index_lotes[quadra] = {}
			lotes_filt = self.lotes[self.lotes['quadra'] == quadra]
			id_lote = 0
			for lote in lotes_filt.lote.unique():
				self.index_lotes[quadra][lote] = id_lote
				id_lote += 1
			id_quadra += 1

	def proc_posicoes(self):
		"""Adiciona as colunas idQuadra e idLote ao dataframe de pontos."""
		getid_q = np.vectorize(lambda q: self.index_quadras[q])
		getid_l = np.vectorize(lambda q, l: self.index_lotes[q][l])
		self.pontos['idLote'] = getid_l(self.pontos.quadra, self.pontos.lote)
		self.pontos['idQuadra'] = getid_q(self.pontos.quadra)

	def proc_fronteiras(self):
		fronteiras = self.viz[self.viz['quadra'] != '0000']
		fronteiras = fronteiras[fronteiras['q2'] == '0000']
		fronteiras.reset_index(drop=True, inplace=True)
		getid_l = np.vectorize(lambda l: self.index_lotes['0000'][l])
		fronteiras['rua'] = getid_l(fronteiras.l2)
		self.fronteiras = fronteiras

	def proc_vizinhancas(self):
		"""Insere os valores de índice nas colunas q2 e l2 das vizinhanças."""
		getid_q = np.vectorize(lambda q: self.index_quadras[q])
		getid_l = np.vectorize(lambda q, l: self.index_lotes[q][l])
		self.viz['id_l2'] = getid_l(self.viz.q2, self.viz.l2)
		self.viz['id_q2'] = getid_q(self.viz.q2)

	def proc_esquinas(self):
		"""Insere os valores de índice na coluna l2 das esquinas."""
		getid_l = np.vectorize(lambda l: self.index_lotes['0000'][l])
		self.esquinas['l2'] = getid_l(self.esquinas.l2)

	def proc_centroides_esquinas(self):
		"""Insere os valores de índice na coluna l2 dos centroides."""
		getid_l = np.vectorize(lambda l: self.index_lotes['0000'][l])
		self.centroides['id_l2'] = getid_l(self.centroides.l2)

	def gerar_arquivo_saida(self):
		print('Gerando arquivo 0-AMB.csv')
		with open(self.simul+'/0-AMB.csv', 'a') as file:
			stdout = sys.stdout
			sys.stdout = file
			self.saida_index_quadras()
			self.saida_vizinhancas()
			self.saida_posicoes()
			self.saida_fronteiras()
			self.saida_esquinas()
			self.saida_centroides_esquinas()
			sys.stdout = stdout
		
	def saida_index_quadras(self):
		print('quantQuadras, quantLotes e indexQuadras')
		index_quadras = []
		quant_lotes = []
		desl = 0
		for quadra in self.lotes.quadra.unique():
			index_quadras.append(desl)
			quant = len(self.lotes[self.lotes['quadra'] == quadra])
			quant_lotes.append(quant)
			desl += quant
			index_quadras.append(desl)
			desl += 1
		print(len(self.lotes.quadra.unique()))
		print_arr_np(quant_lotes)
		print_arr_np(index_quadras)

	def saida_vizinhancas(self):
		print('\nindexVizinhancas e vetorVizinhancas')
		print_arr_np(self.gerar_indice(self.viz))
		viz_saida = self.viz.drop(['quadra', 'lote', 'q2', 'l2'], axis=1)
		print_arr_np(viz_saida.values.flatten())

	def saida_posicoes(self):
		print('\nindexPosicoes, vetorPosicoes e indexPosicoesRegioes')
		print_arr_np(self.gerar_indice(self.pontos))		
		pontos_saida = self.pontos.drop(['id', 'lote', 'quadra'], axis=1)
		print_arr_np(pontos_saida.values.flatten())
		print_arr_np(self.gerar_indice_regioes())

	def saida_fronteiras(self):
		print('\nindexFronteiras e vetorFronteiras')
		fronteiras = self.fronteiras.copy()
		print_arr_np(self.gerar_indice(fronteiras))
		cols_to_drop = ['quadra', 'lote', 'x1', 'y1', 'q2', 'l2']
		fronteiras.drop(columns=cols_to_drop, inplace=True)
		print_arr_np(fronteiras.values.flatten())

	def saida_esquinas(self):
		print('\nindexEsquinas e vetorEsquinas')
		print_arr_np(self.gerar_indice_por_lotes(self.esquinas))
		esquinas = self.esquinas.drop(['lote'], axis=1)
		print_arr_np(esquinas.values.flatten())

	def saida_centroides_esquinas(self):
		print('\nindexCentrosEsquinas e vetorCentrosEsquinas')
		print_arr_np(self.gerar_indice_por_lotes(self.centroides))
		centroides = self.centroides.drop(['lote', 'l2'], axis=1)
		print_arr_np(centroides.values.flatten())

	def gerar_indice(self, df):
		"""Retorna um índice por quadra e lote de um DataFrame."""
		indice = df.groupby(['quadra', 'lote']).size()
		indice = pd.DataFrame(indice, columns=['qtde'])
		indice.reset_index(inplace=True)
		# Adiciona uma entrada vazia no início de cada quadra
		last = len(indice)
		for quadra in self.lotes.quadra.unique():
			entry = {'quadra': quadra, 'lote': '0', 'qtde': 0}
			indice.loc[last] = entry
			last += 1
			# Adiciona entradas vazias para lotes inexistentes
			filt = indice[indice['quadra'] == quadra]
			lotes_filt = filt.lote.unique()
			for lote in self.index_lotes[quadra].keys():
				if lote not in lotes_filt:
					entry = {'quadra': quadra, 'lote': lote, 'qtde': 0}
					indice.loc[last] = entry
					last += 1
		# Reordena e reindexa o DataFrame
		indice.sort_values(by=['quadra', 'lote'], inplace=True)
		indice.reset_index(inplace=True, drop=True)
		return indice.qtde.values.flatten().cumsum()
	
	def gerar_indice_regioes(self):
		pontos = self.pontos.copy()
		pontos['regiao'] = pontos.quadra.map(get_regiao)
		pontos_por_regiao = pontos.groupby(['regiao']).size()
		pontos_por_regiao = pd.DataFrame(pontos_por_regiao, columns=['qtde'])
		pontos_por_regiao = pontos_por_regiao.to_dict(orient='dict')['qtde']
		indice_regioes = []
		indice_regioes.append(0)
		for regiao in ['RUA', 'RURAL', 'QUADRA']:
			if regiao in pontos_por_regiao.keys():
				indice_regioes.append(pontos_por_regiao[regiao])
			else:
				indice_regioes.append(0)
		indice_regioes = np.array(indice_regioes)
		return indice_regioes.cumsum()

	def gerar_indice_por_lotes(self, df):
		indice = df.groupby(['lote']).size()
		indice = pd.DataFrame(indice, columns=['qtde'])
		indice.reset_index(inplace=True)
		# Adiciona uma entrada vazia
		last = len(indice)
		entry = {'lote': '0', 'qtde': 0}
		indice.loc[last] = entry
		last += 1
		# Adiciona entradas vazias para lotes inexistentes
		lotes_com_esquina = indice.lote.unique()
		for lote in self.index_lotes['0000'].keys():
			if lote not in lotes_com_esquina:
				entry = {'lote': lote, 'qtde': 0}
				indice.loc[last] = entry
				last += 1
		# Reordena e reindexa o DataFrame
		indice.sort_values(by=['lote'], inplace=True)
		indice.reset_index(inplace=True, drop=True)
		return indice.qtde.values.flatten().cumsum()
