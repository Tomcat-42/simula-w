#!/usr/bin/env python3

import pandas as pd
import numpy as np
import sys
import banco

class Distribuicao:

	__slots__ = ['params', 'dist_humanos', 'dist_mosquitos', 'pontos',
				 'casos_humanos', 'casos_mosquitos', 'index_quadras', 'simul']

	def __init__(self, params, amb, simul):
		print('Processando distribuição de agentes infectados...')
		self.simul = simul
		self.params = params
		self.pontos = amb.pontos.copy()
		self.index_quadras = amb.index_quadras
		self.ler_dados()
		self.proc_casos_humanos()
		self.proc_sexo_humanos()
		self.proc_posicoes_humanos()
		self.proc_casos_mosquitos()
		self.proc_posicoes_mosquitos()

	def ler_dados(self):
		"""Acessa os dados de distribuição do banco de dados."""
		ambiente = self.params['Ambiente']
		self.dist_humanos = banco.ler_distribuicao_humanos(ambiente)
		self.dist_mosquitos = banco.ler_distribuicao_mosquitos(ambiente)

	def proc_casos_humanos(self):
		duracao_ano = int(self.params['DuraçãoAno'])
		prc_humanos = float(self.params['PorcentagemCasosHumanos'])
		df = self.dist_humanos.copy()
		self.casos_humanos = None
		for i in range(4):  # Divide o vetor de distribuição em 4 partes
			inicio = int(duracao_ano * i / 4)
			fim = int(duracao_ano * (i + 1) / 4)
			# Filtra os casos correspondentes ao período selecionado
			df_filt = df[(df['ciclo'] >= inicio) & (df['ciclo'] < fim)]
			df_filt = df_filt.sample(frac=prc_humanos)
			# Constrói uma nova tabela com uma fração dos casos
			if self.casos_humanos is None: 
				self.casos_humanos = df_filt
			else:
				self.casos_humanos = self.casos_humanos.append(df_filt)
		self.casos_humanos.sort_values(by=['ciclo'], inplace=True)
		self.casos_humanos.reset_index(drop=True, inplace=True)

	def proc_sexo_humanos(self):
		df = self.casos_humanos.copy()
		self.casos_humanos = None
		fracao_pop = {  # Dicionário de faixas etárias e frações
			'B': float(self.params['FraçãoBebesMasculinos']),
			'C': float(self.params['FraçãoCriançasMasculinas']),
			'D': float(self.params['FraçãoAdolescentesMasculinos']),
			'J': float(self.params['FraçãoJovensMasculinos']),
			'A': float(self.params['FraçãoAdultosMasculinos']),
			'I': float(self.params['FraçãoIdososMasculinos'])
		}
		for fe, frac in fracao_pop.items():  # Para cada faixa etária
			df_filt = df[df['faixa_etaria'] == fe].copy()
			if len(df_filt > 0):
				df_filt = df_filt.sample(frac=1.0)
			arr_sexo = ['F' for i in range(len(df_filt))]
			# Atribui o sexo de acordo com as frações populacionais
			for i in range(int(round(len(df_filt) * frac))):
				arr_sexo[i] = 'M'
			df_filt['sexo'] = arr_sexo
			# Constrói uma nova tabela com a coluna sexo
			if self.casos_humanos is None: 
				self.casos_humanos = df_filt
			else:
				self.casos_humanos = self.casos_humanos.append(df_filt)
		self.casos_humanos.sort_values(by=['ciclo'], inplace=True)
		self.casos_humanos.reset_index(drop=True, inplace=True)

	def proc_posicoes_humanos(self):
		id_pontos = []
		for i in range(len(self.casos_humanos)):
			xp = self.casos_humanos.loc[i].x
			yp = self.casos_humanos.loc[i].y
			func = np.vectorize(lambda x, y: (x - xp) ** 2 + (y - yp) ** 2)
			self.pontos['distancia'] = func(self.pontos.x, self.pontos.y)
			idxmin = self.pontos.distancia.idxmin()
			id_pontos.append(idxmin)
		self.pontos.drop(columns=['distancia'], inplace=True)
		self.casos_humanos['id_ponto'] = id_pontos

	def proc_casos_mosquitos(self):
		# Leitura dos parâmetros
		duracao_ano = int(self.params['DuraçãoAno'])
		prc_mosquitos = float(self.params['PorcentagemCasosFemeas'])
		df = self.dist_humanos.copy()
		casos = None
		for i in range(4):  # Divide o vetor de distribuição em 4 partes
			inicio = int(duracao_ano * i / 4)
			fim = int(duracao_ano * (i + 1) / 4)
			# Filtra os casos correspondentes ao período selecionado
			df_filt = df[(df['ciclo'] >= inicio) & (df['ciclo'] < fim)]
			df_filt = df_filt.sample(frac=prc_mosquitos)
			# Constrói uma nova tabela com uma fração dos casos
			if casos is None: 
				casos = df_filt
			else:
				casos = casos.append(df_filt)
		# Completa os parâmetros dos casos
		casos.drop(columns=['x', 'y', 'faixa_etaria'], inplace=True)
		casos['quadra'] = list(self.pontos.sample(n=len(casos)).quadra)
		casos['quantidade_total'] = 1
		casos['sexo'] = 'F'
		casos['fase'] = 'A'
		casos['probabilidade_minimo_infectados'] = 1.0
		casos['probabilidade_maximo_infectados'] = 1.0
		casos['sorotipo'] = 2
		# Ajusta os casos e os adiciona ao final da lista
		casos = casos.sample(frac=1.0)
		self.casos_mosquitos = self.dist_mosquitos.append(casos,
			ignore_index=True)
		self.casos_mosquitos.reset_index(drop=True, inplace=True)

	def proc_posicoes_mosquitos(self):
		getid_q = np.vectorize(lambda q: self.index_quadras[q])
		self.casos_mosquitos.quadra = getid_q(self.casos_mosquitos.quadra)

	def gerar_arquivo_saida_humanos(self):
		print('Gerando arquivo DistribuicaoHumanos.csv')
		with open(self.simul+'/DistribuicaoHumanos.csv', 'w') as file:
			stdout = sys.stdout
			sys.stdout = file
			self.saida_distribuicao_humanos()
			sys.stdout = stdout

	def saida_distribuicao_humanos(self):
		df = self.formatar_saida_humanos()
		print(len(df))
		print(df.to_csv(sep=';', index=False))

	def gerar_arquivo_saida_mosquitos(self):
		print('Gerando arquivo DistribuicaoMosquitos.csv')
		with open(self.simul+'/DistribuicaoMosquitos.csv', 'w') as file:
			stdout = sys.stdout
			sys.stdout = file
			self.saida_distribuicao_mosquitos()
			sys.stdout = stdout

	def saida_distribuicao_mosquitos(self):
		df = self.formatar_saida_mosquitos()
		print(len(df))
		print(df.to_csv(sep=';', index=False))

	def formatar_saida_humanos(self):
		df = self.casos_humanos.copy()
		# Cria funções de acesso à tabela de pontos
		getid_q = np.vectorize(lambda id_: self.pontos.loc[id_].idQuadra)
		getid_l = np.vectorize(lambda id_: self.pontos.loc[id_].idLote)
		get_x = np.vectorize(lambda id_: self.pontos.loc[id_].x)
		get_y = np.vectorize(lambda id_: self.pontos.loc[id_].y)
		# Cria as colunas da tabela de saída
		df['Q'] = getid_q(self.casos_humanos.id_ponto)
		df['L'] = getid_l(self.casos_humanos.id_ponto)
		df['X'] = get_x(self.casos_humanos.id_ponto)
		df['Y'] = get_y(self.casos_humanos.id_ponto)
		df['Sexo'] = self.casos_humanos['sexo']
		df['FaixaEtaria'] = self.casos_humanos['faixa_etaria']
		df['SaudeDengue'] = 'I'
		df['SorotipoAtual'] = 2
		df['Ciclo'] = self.casos_humanos['ciclo']
		# Descarta as colunas desnecessárias
		to_drop = ['ciclo', 'x', 'y', 'faixa_etaria', 'sexo', 'id_ponto']
		df.drop(columns=to_drop, inplace=True)
		return df

	def formatar_saida_mosquitos(self):
		df = self.casos_mosquitos.copy()
		columns = {
			'quadra': 'Quadra',
			'quantidade_total': 'QuantidadeTotal',
			'sexo': 'Sexo',
			'fase': 'Fase',
			'probabilidade_minimo_infectados': 'PercentualMinimoInfectados',
			'probabilidade_maximo_infectados': 'PercentualMaximoInfectados',
			'sorotipo': 'Sorotipo',
			'ciclo': 'Ciclo'
		}
		# Renomeia e reordena as colunas
		df.rename(index=str, columns=columns, inplace=True)
		return df[list(columns.values())]
