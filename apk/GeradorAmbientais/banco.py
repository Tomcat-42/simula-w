#!/usr/bin/env python3

import sqlalchemy as sql
import pandas as pd

QUERY_PARAMETROS = """
	SELECT key, value FROM {0};
"""

"""Consultas utilizadas na geração do arquivo 0-AMB.csv"""

QUERY_PONTOS = """
	SELECT id, x, y, lote, quadra FROM {0}_pontos
	ORDER BY quadra, lote;
"""

QUERY_LOTES = """
	SELECT DISTINCT quadra, lote FROM {0}_poligonos
	ORDER BY quadra, lote;
"""

QUERY_VIZINHANCAS = """
	SELECT q1 AS quadra, l1 AS lote, x1, y1, x2, y2, l2, q2
	FROM {0}_vizinhancasmoorepontos;
"""

QUERY_ESQUINAS = """
	SELECT l1 AS lote, x, y, l2 FROM {0}_pontosesquinas
	ORDER BY lote;
"""

QUERY_CENTR_ESQUINAS = """
	SELECT l1 AS lote, x, y, l2 FROM {0}_centroidesesquinas
	ORDER BY lote;
"""

"""Consultas utilizadas na geração do arquivo 1-MOV.csv"""

QUERY_POLIGONOS = """
	SELECT quadra, lote FROM {0}_poligonos;
"""

QUERY_TIPOS_TRAJETOS = """
	SELECT faixa_etaria, locais, periodos FROM tipos_trajetos;
"""

QUERY_ARESTAS = """
	SELECT q1, l1, q2, l2 FROM {0}_arestas ORDER BY q1, l1, q2, l2;
"""

QUERY_CENTR_LOTES = """
	SELECT q, l, x, y FROM {0}_centroideslotes;
"""

"""Consultas utilizadas na geração do arquivo 2-CON.csv"""

QUERY_QUADRAS_CONTROLE_BIO = """
	SELECT * FROM {0}_dengue_quadrascontrolebiologico;
"""

QUERY_QUADRAS_CONTROLE_AMB = """
	SELECT * FROM {0}_dengue_quadrascontroleambiental;
"""

QUERY_QUADRAS_VACINACAO = """
	SELECT * FROM {0}_dengue_quadrasvacinacao;
"""

QUERY_FE_VACINACAO = """
	SELECT * FROM {0}_dengue_faixasetariasvacinacao;
"""

QUERY_CICLOS_VACINACAO = """
	SELECT * FROM {0}_dengue_ciclosvacinacao;
"""

QUERY_PONTOS_ESTRATEGICOS = """
	SELECT * FROM {0}_dengue_lotespontosestrategicos;
"""

QUERY_NIVEIS_INFESTACAO = """
	SELECT * FROM {0}_dengue_infestacaomosquitos;
"""

QUERY_INFO_CONTROLES = """
	SELECT quadra, (ciclo_inicio + ciclo_termino) / 2 AS ciclo, tipo_controle,
	taxa_min_mecanico, taxa_max_mecanico, taxa_min_quimico, taxa_max_quimico
	FROM {0}_dengue_controles;
"""

QUERY_PONTOS_CONTROLES = """
	SELECT id_controle, x, y, lote, quadra
	FROM {0}_dengue_controles_pontos ORDER BY id_controle;
"""

QUERY_PONTOS_RAIOS = """
	SELECT id_controle, x, y, lote, quadra
	FROM {0}_dengue_raios ORDER BY id_controle;
"""

QUERY_VACINADOS = """
	SELECT * FROM {0}_dengue_vacinados;
"""

"""Consultas utilizadas na geração do arquivo 3-CLI.csv"""

QUERY_CLIMATICOS = """
	SELECT * FROM {0}_dengue_climaticos;
"""

"""Consultas utilizadas na geração dos arquivos de distribuição"""

QUERY_DIST_HUMANOS = """
	SELECT * FROM {0}_dengue_distribuicaohumanos;
"""

QUERY_DIST_MOSQUITOS = """
	SELECT * FROM {0}_dengue_distribuicaomosquitos;
"""

database_engine = None  # Instância global de conexão ao banco de dados 
database_engine_simula = None 

def get_database_engine():
	global database_engine
	if database_engine is None:  # Cria a conexão ao banco de dados PostGres
		database_url = "postgresql://postgres:postgres@localhost/BaseGeo"
		database_engine = sql.create_engine(database_url)
	return database_engine

def get_database_engine_simula():
	global database_engine_simula
	if database_engine_simula is None:  # Cria a conexão ao banco de dados PostGres
		database_url_simula = "postgresql://postgres:postgres@localhost/simula-w"
		database_engine_simula = sql.create_engine(database_url_simula)
	return database_engine_simula

def ler_parametros(simul):
	query = QUERY_PARAMETROS.format(simul+"_ambientais");
	df = pd.read_sql(query, get_database_engine_simula())
	params = {}
	for index, row in df.iterrows():
		params[row.key] = row.value
	return params

def ler_pontos(ambiente):
	query = QUERY_PONTOS.format(ambiente)
	df = pd.read_sql(query, get_database_engine())
	df.x = df.x.astype(int)
	df.y = df.y.astype(int)
	return df

def ler_lotes(ambiente):
	query = QUERY_LOTES.format(ambiente)
	df = pd.read_sql(query, get_database_engine())
	return df

def ler_vizinhancas(ambiente):
	query = QUERY_VIZINHANCAS.format(ambiente)
	viz = pd.read_sql(query, get_database_engine())
	# Converte as colunas de coordenadas para inteiros
	viz.x1 = viz.x1.astype(int)
	viz.y1 = viz.y1.astype(int)
	viz.x2 = viz.x2.astype(int)
	viz.y2 = viz.y2.astype(int)
	# Cria uma cópia da tabela invertendo os pontos de origem e destino
	nome_colunas = {'lote': 'l2', 'quadra': 'q2', 'x1': 'x2', 'y1': 'y2',
					'x2': 'x1', 'y2': 'y1', 'l2': 'lote', 'q2': 'quadra'}
	ordem_colunas = ['quadra', 'lote', 'x1', 'y1', 'x2', 'y2', 'l2', 'q2']
	copy = viz.rename(columns=nome_colunas)
	viz = viz.reindex(columns=ordem_colunas)
	copy = copy.reindex(columns=ordem_colunas)
	viz.reset_index(inplace=True)
	copy.reset_index(inplace=True)
	# Concatena as tabelas e ordena por quadra e lote
	df = viz.append(copy)
	df = df.sort_values(by=['quadra', 'lote', 'q2', 'l2', 'index'])
	df.drop(columns=['index'], inplace=True)
	df.reset_index(drop=True, inplace=True)
	return df

def ler_esquinas(ambiente):
	query  = QUERY_ESQUINAS.format(ambiente)
	df = pd.read_sql(query, get_database_engine())
	df.x = df.x.astype(int)
	df.y = df.y.astype(int)
	return df

def ler_centroides_esquinas(ambiente):
	query = QUERY_CENTR_ESQUINAS.format(ambiente)
	df = pd.read_sql(query, get_database_engine())
	df.x = df.x.astype(int)
	df.y = df.y.astype(int)
	return df

def ler_poligonos(ambiente):
	query = QUERY_POLIGONOS.format(ambiente)
	df = pd.read_sql(query, get_database_engine())
	lotes = df[df.quadra.str.startswith('Q', na=False)]
	vertices = df[df.quadra.str.startswith('0', na=False)]
	return lotes, vertices

def ler_tipos_trajetos():
	query = QUERY_TIPOS_TRAJETOS
	df = pd.read_sql(query, get_database_engine())
	tipos_trajetos = {}
	for fe in df.faixa_etaria.unique():
		tipos_trajetos[fe] = df.query('faixa_etaria == @fe')
	return tipos_trajetos

def ler_arestas(ambiente):
	query = QUERY_ARESTAS.format(ambiente)
	df = pd.read_sql(query, get_database_engine())
	return df

def ler_centroides_lotes(ambiente):
	query = QUERY_CENTR_LOTES.format(ambiente)
	df = pd.read_sql(query, get_database_engine())
	df.x = df.x.astype(int)
	df.y = df.y.astype(int)
	return df

def ler_quadras_controle_bio(ambiente):
	query = QUERY_QUADRAS_CONTROLE_BIO.format(ambiente)
	df = pd.read_sql(query, get_database_engine())
	return df

def ler_quadras_controle_amb(ambiente):
	query = QUERY_QUADRAS_CONTROLE_AMB.format(ambiente)
	df = pd.read_sql(query, get_database_engine())
	return df

def ler_quadras_vacinacao(ambiente):
	query = QUERY_QUADRAS_VACINACAO.format(ambiente)
	df = pd.read_sql(query, get_database_engine())
	return df

def ler_fe_vacinacao(ambiente):
	query = QUERY_FE_VACINACAO.format(ambiente)
	df = pd.read_sql(query, get_database_engine())
	return df

def ler_ciclos_vacinacao(ambiente):
	query = QUERY_CICLOS_VACINACAO.format(ambiente)
	df = pd.read_sql(query, get_database_engine())
	return df

def ler_pontos_estrategicos(ambiente):
	query = QUERY_PONTOS_ESTRATEGICOS.format(ambiente)
	df = pd.read_sql(query, get_database_engine())
	return df

def ler_niveis_infestacao(ambiente):
	query = QUERY_NIVEIS_INFESTACAO.format(ambiente)
	df = pd.read_sql(query, get_database_engine())
	return df

def ler_info_controles(ambiente):
	query = QUERY_INFO_CONTROLES.format(ambiente)
	df = pd.read_sql(query, get_database_engine())
	return df

def ler_pontos_controles(ambiente):
	query = QUERY_PONTOS_CONTROLES.format(ambiente)
	df = pd.read_sql(query, get_database_engine())
	df.x = df.x.astype(int)
	df.y = df.y.astype(int)
	return df

def ler_pontos_raios(ambiente):
	query = QUERY_PONTOS_RAIOS.format(ambiente)
	df = pd.read_sql(query, get_database_engine())
	df.x = df.x.astype(int)
	df.y = df.y.astype(int)
	return df

def ler_vacinados(ambiente):
	query = QUERY_VACINADOS.format(ambiente)
	df = pd.read_sql(query, get_database_engine())
	return df

def ler_climaticos(ambiente):
	query = QUERY_CLIMATICOS.format(ambiente)
	df = pd.read_sql(query, get_database_engine())
	return df

def ler_distribuicao_humanos(ambiente):
	query = QUERY_DIST_HUMANOS.format(ambiente)
	df = pd.read_sql(query, get_database_engine())
	return df

def ler_distribuicao_mosquitos(ambiente):
	query = QUERY_DIST_MOSQUITOS.format(ambiente)
	df = pd.read_sql(query, get_database_engine())
	return df
